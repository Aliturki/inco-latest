<?php

//Website
Route::group(['namespace'=>'Website'],function (){
    Route::get('/','HomeController@index');
    Route::get('/service/{id}','HomeController@service_show')->name('service::show');
    // Blog
    Route::get('/blog','HomeController@blogIndex')->name('blog::index');
    Route::get('/article/{id}','HomeController@article_show')->name('article::show');
    Route::get('/contact-us','HomeController@contact_show')->name('contact::show');
    Route::post('/contact-us','HomeController@contact_store')->name('contact::store');
//    Pages
    Route::get('/page/{page}','HomeController@pageShow')->name('page::show');
});


Auth::routes();
//APIs Initialize
Route::group(['middleware'=>'isAdmin','namespace'=>'Dashboard','prefix'=>'dashboard/api'],function(){
    //Setting
    Route::get('/site_settings','DashboardController@siteSettings');
    Route::post('/site_settings','DashboardController@saveSettings');
    //User Edit
    Route::get('/user/edit','DashboardController@userEdit');
    Route::post('/user/update','DashboardController@userUpdate');
    Route::post('/user/upload-pic','DashboardController@storeUserImage');
    Route::get('/user/profile-pic','DashboardController@getProfilePic');
    //Slider
    Route::post('/slider/delete','SliderController@delete');
    Route::resource('/slider','SliderController');
    Route::post('/slider/{id}/update','SliderController@update');
    Route::get('/slider/activation/{id}','SliderController@activation');
    //article
    Route::post('/article/delete','ArticleController@delete');
    Route::resource('/article','ArticleController');
    Route::post('/article/{id}/update','ArticleController@update');
    Route::get('/article/activation/{id}','ArticleController@activation');
    //Service
    Route::resource('/service','ServiceController');
    Route::post('/service/{id}/update','ServiceController@update');
    Route::post('/service/delete','ServiceController@delete');
    //Testimonial
    Route::resource('/testimonial','TestimonialController');
    Route::post('/testimonial/{id}/update','TestimonialController@update');
    Route::post('/testimonial/delete','TestimonialController@delete');
    //Subscribers
    Route::get('/subscribe','SubscribeController@index');
    Route::post('/subscribe/delete','SubscribeController@delete');
    //Contact us
    Route::get('/messages','DashboardController@getMessages');
    Route::delete('/message/{id}','DashboardController@deleteMessage');
    Route::get('/message/{id}/read','DashboardController@readMessage');
    Route::get('/message/count','DashboardController@getMessageCount');
    // page
    Route::resource('/page','StaticPageController');
    Route::post('/page/{id}/update','StaticPageController@update');
    Route::post('/page/delete','StaticPageController@delete');
    //Stor Uploded images that is comming from Froala Text Editor
    Route::post('/upload-image','DashboardController@uploadImage');
    //    delete Uploded images that is comming from Froala Text Editor
    Route::post('/delete-image','DashboardController@deleteImage');
    // Dashboard Counter
    Route::get('/models/counter','DashboardController@dashboardCounter');
    //Logout
    Route::post('/logout','DashboardController@logout');
});

Route::group(['middleware'=>'isAdmin','namespace'=>'Dashboard'],function(){

    Route::any('/dashboard/{any?}',function(){
        return view('backend.home');
    })->where('any','.*');


});


Route::get('/home', 'HomeController@index');
