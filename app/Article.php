<?php

namespace App;

use App\BaseModel;

class Article extends BaseModel
{
    protected $guarded = [];

    protected $filter = [
        'id','title','content','author','created_at'
    ];

    public static function form(){
        return [
            'title'=>'',
            'author'=>'',
            'content'=>'',
            'small_content'=>'',
            'image'=>'',
            'is_active'=>true
        ];
    }


    public function getContentAttribute($val){
        return stripcslashes($val);
    }

    public function getIsActiveAttribute($value){
        return $value ? true : false;
    }
}
