<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    public function getKeywordsAttribute($value){
        return explode(',',$value);
    }
    public function getStatusAttribute($value)
    {
        return $value ? true : false;
    }
}
