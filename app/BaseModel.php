<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Support\FilterPaginateOrder;
class BaseModel extends Model
{

    use FilterPaginateOrder;

}