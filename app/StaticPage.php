<?php

namespace App;
use Carbon\Carbon;

class StaticPage extends BaseModel
{

    protected $guarded = [];

    protected $filter = [
        'id','title','slug','content','is_active','created_at'
    ];


    public static function form(){
        return [
            'title'=>'',
            'slug'=>'',
            'content'=>'',
            'is_active'=>true
        ];
    }

    public function getCreatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->toDateString();
    }


    public function getIsActiveAttribute($value)
    {
        return $value ? true : false;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

}
