<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['diff_humans'];


    public function getDiffHumansAttribute(){
        $this->created_at->setLocale('ar');
        return $this->created_at->diffForHumans();
    }
}
