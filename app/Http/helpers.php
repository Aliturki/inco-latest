<?php


function inco_allow_tags(){
    return '<body><br><button><canvas><caption><center><cite><code><col><colgroup><command><content>
<data><datalist><dd><del><details><dfn><dialog><dir><div><dl><dt><element><em><embed>
<fieldset><figcaption><figure><font><footer><head><header><hgroup><hr>
<html><i><image><img><input><ins><isindex><kbd><keygen><label><legend><li><link><listing>
<main><map><mark>
<marquee><menu><menuitem><meta><meter><multicol><nav><nobr><noembed><noframes><noscript><object>
<ol><optgroup><option><output><p><param><picture><plaintext><pre><progress><q><rp><rt><rtc><ruby>
<s><samp>
<section><select><shadow><slot><small><source><spacer>
<span><strike><strong><style><sub><summary><sup><table><tbody><td><template><textarea>
<tfoot><th><thead><time><title><tr><track><tt><u><ul><var><blockquote><h1><h2><h3><h4><h5><h6>';

}

function inco_felter_content($field){
    return strip_tags(request()->input($field), inco_allow_tags());
}


function inco_activation($activation){
    if($activation == 'true'){
        return 1;
    }elseif ($activation == 'false'){
        return 0;
    }
    return null;
}


function getSetting(){
    return DB::table('site_settings')->first();
}


function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}


function getServices(){
    return DB::table('services')->select(['id','title'])->get();
}
function getAbout(){
    return DB::table('static_pages')->where('id',7)->first(['slug']);
}
