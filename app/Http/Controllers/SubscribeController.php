<?php

namespace App\Http\Controllers;

use App\Subscribe;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{

    public function subscribe(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:subscribes'
        ]);

       $sub = new Subscribe($request->all());
       $sub->ip = $request->ip();
       $sub->save();
        return back();
    }
}
