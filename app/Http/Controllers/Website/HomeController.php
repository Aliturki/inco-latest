<?php

namespace App\Http\Controllers\Website;

use App\Article;
use App\Contactus;
use App\Service;
use App\SiteSetting;
use App\Slider;
use App\StaticPage;
use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class HomeController extends Controller
{
    public function index(){
        $sliders = Slider::where('is_active',1)->orderBy('created_at','desc')->get(['id','title','image','description','url']);
        $settings = SiteSetting::where('status',1)->first();
        $services = Service::where('is_active',1)->orderBy('id','desc')->get();
        $testimonial = Testimonial::where('is_active',1)->orderBy('id','desc')->get();
        $articles = Article::where('is_active',1)->orderBy('created_at','desc')->limit(3)->get(['id','title','created_at','small_content','image']);
        $roeytna = StaticPage::find(5);
        $features = StaticPage::find(6);
        $about = StaticPage::where('id',7)->first(['slug']);
        return view('website.home',compact('sliders','settings','services','roeytna','testimonial','articles','features','about'));

    }


    //Services
    public function service_index(){
        return view('website.services.index');
    }

    public function service_show($id){

        $service = Service::find($id);

        if($service){
            return view('website.services.show',compact('service'));
        }
        return back();
    }

    public function article_show($id){
        $titles = Article::where('is_active',1)->orderBy('created_at','desc')->limit(5)->get(['id','title']);
        $article = Article::find($id);

        if($article){
            return view('website.article.show',compact('article','titles'));
        }
        return back();
    }



    public function contact_show(){
        return view('website.contactus');
    }

    public function contact_store(Request $request){

        $custom_ms = [
            'name.required'=>'الإسم مطلوب',
            'name.max'=>'يجب أن لا يحتوي الاسم على أكثر من 25 حرف',
            'name.min'=>'يجب أن يتكون الأسم على 3 أحرف او أكثر',
            'subject.max'=>'يجب أن لا يحتوي موضوع الرساله على أكثر من 150 حرف',
            'subject.required'=>'أخبرنا بموضوع رسالتك',
            'subject.min'=>'يجب أن يتكون موضوع الرساله 6 حروف على الأقل',
            'email.required'=>'من فضلك أدخل بريدك الإلكتروني',
            'email.email'=>'أدخل بريد إلكتروني صحيح',
            'message.required'=>'أدخل محتوي الرساله',
            'message.max'=>'لايمكن ان يحتوي موضوع الرساله على أكثر من 500 حرف',
            'phone.regex'=>'أدخل رقم هاتف صحيح',
            'phone.min'=>'أدخل رقم هاتف صحيح',
            'phone.max'=>'أدخل رقم هاتف صحيح',
        ];

        Validator::make($request->all(),[
            'name'=>'required|max:25|min:3',
            'email'=>'email|required',
            'subject'=>'max:150|min:6|required',
            'phone'=>'min:11|max:14|regex:/(01)[0-9]{9}/',
            'message'=>'required'

        ],$custom_ms)->validate();

        $contact = new Contactus($request->all());
        $contact->ip = $request->ip();
        $contact->saveOrFail();
        return back()->with('success','تم إرسال رسالتك بنجاح');
    }

    public function blogIndex(){
        $articles = Article::where('is_active',1)->paginate(9);
        return view('website.article.index',compact('articles'));
    }

//    Pages

    public function pageShow(StaticPage $page){

        return view('website.page-show',compact('page'));
    }
}
