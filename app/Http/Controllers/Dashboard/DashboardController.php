<?php

namespace App\Http\Controllers\Dashboard;

use App\Article;
use App\Contactus;
use App\Service;
use App\SiteSetting;
use App\Slider;
use App\StaticPage;
use App\Testimonial;
use Auth;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use Validator;
class DashboardController extends Controller
{
    public function index()
    {
        return view('backend.layouts.master');
    }


    public function siteSettings()
    {
        return response()->json([
            'form' => SiteSetting::first()
        ]);
    }

    public function saveSettings(Request $request)
    {
        $custom_msg = [
            'site_name.required'=>'ادخل عنوان الموقع',
            'email.required'=>'أدخل البريد الإلكتروني',
            'email.email'=>'ادخل بريد الكتروني صحيح',
            'description.required'=>'ادخل وصف الموقع',
            'description.max'=>'الوصف يجب ان يتكون 160 حرف',
            'description.min'=>'الوصف يجب ان يتكون 160 حرف',
            'keywords.required'=>'ادخل الكلمات المفتاحيه الموقع',
            'keywords.max'=>'الكلمات المفتاحيه يجب أن تتكون من 160 حرف',
//            'keywords.min'=>'الكلمات المفتاحيه يجب ان تتكون 160 حرف',

        ];
        Validator::make($request->all(), [
            'site_name' => 'required',
            'email' => 'required|email',
            'description' => 'required|min:160|max:160',
            'keywords' => 'required|max:160',
        ],$custom_msg)->validate();

        $setting = SiteSetting::find(1);
        $setting->site_name = $request->site_name;
        $setting->keywords = implode(',', $request->keywords);
        $setting->description = $request->description;
        $setting->address = $request->address;
        $setting->facebook = $request->facebook;
        $setting->phone = $request->phone;
        $setting->email = $request->email;
        $setting->status = inco_activation($request->status);
        $setting->update();
        return response(['saved' => true]);
    }


    public function userEdit()
    {
        $user = Auth::user();

        return response()->json([
            'form' => $user
        ]);
    }

    public function userUpdate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:20',
            'email' => 'email|required',
            'password' => 'min:6|max:25|confirmed|required_with:old_password|nullable',
            'old_password' => 'required_with:password_confirmation|nullable',
        ]);


        if ($request->has('password')) {
            if (!Hash::check($request->old_password, Auth::user()->password)) {
                return response()->json(['invalid_password' => ['كلمة المرور  خاطئه']], 422);
            }
            Auth::user()->password = Hash::make($request->password);

        }


        Auth::user()->name = $request->name;
        Auth::user()->email = $request->email;
        Auth::user()->update();


        return response()
            ->json([
                'success'=>true
            ]);
    }

    public function storeUserImage(Request $request){
        $data = $request->image;
        list($type,$data) = explode(';',$data);
        list(,$data) = explode(',',$data);
        $data = base64_decode($data);
        $filename = time().'-'.str_random(6).'.png';
        $path = public_path('/uploads/'.$filename);
        file_put_contents($path,$data);
        File::delete(public_path('/uploads/').Auth::user()->image);
        Auth::user()->image = $filename;
        Auth::user()->update();
        return response()->json(['pic'=>$filename]);
    }

    // Messages
    public function getMessages(){
        return response()->json([
            'model'=>Contactus::orderBy('created_at','desc')->get()
        ]);
    }

    public function deleteMessage(Contactus $id){
        if($id){
            $id->delete();
            return response()->json(['deleted'=>true]);
        }

        return response()->json(['هذا المحتوى غير موجود'],404);

    }

    public function readMessage(Contactus $id){
        $id->_read = 1;
        $id->save();
        return response()->json(['read'=>true]);
    }

    public function getMessageCount(){
        return response()->json([
            'msg_cont'=> Contactus::where('_read',0)->count()
        ]);
    }


    public function dashboardCounter(){
        return response()->json([
            'articles'=>Article::count(),
            'services'=>Service::count(),
            'sliders'=>Slider::count(),
            'testimonials'=>Testimonial::count(),
            'pages'=>StaticPage::count(),
            'messages'=>Contactus::where('_read','0')->count(),
        ]);
    }

    public function logout(){
        Auth::logout();
        return response()->json(['success'=>true]);
    }
}
