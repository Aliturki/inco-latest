<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use App\Testimonial;
use Illuminate\Http\Request;
use Validator;

class TestimonialController extends Controller
{

    public function index()
    {
        return response()->json([
            'model' => Testimonial::filterPaginateOrder()
        ]);
    }


    public function create()
    {
        return response()
            ->json([
                'form' => Testimonial::form()
            ]);
    }

    public function store(Request $request)
    {

        $message = [
            'speaker.required' => 'أدخل الأسم',
            'content.required' => ' أدخل المحتوي'
        ];
        Validator::make($request->all(), [
            'speaker' => 'required',
            'content' => 'required'
        ], $message)->validate();

        $t = new Testimonial($request->all());

        $t->is_active = ($request->is_active === 'true' ? 1 : 0);

        $t->save();
        return response()
            ->json(['success' => true]);
    }


    public function edit($id)
    {
        $form = Testimonial::findOrFail($id);

        return response()
            ->json([
                'form' => $form
            ]);
    }


    public function update(Request $request, $id)
    {

        $message = [
            'speaker.required' => 'أدخل الأسم',
            'content.required' => ' أدخل المحتوي'
        ];
        Validator::make($request->all(), [
            'speaker' => 'required',
            'content' => 'required'
        ], $message)->validate();

        $t = Testimonial::findOrFail($id);
        $t->speaker = $request->input('speaker');
        $t->content = $request->input('content');
        $t->is_active = ($request->is_active === 'true' ? 1 : 0);
        $t->update();
        return response()
            ->json(['success' => true]);
    }


    public function delete(Request $request)
    {
        Testimonial::whereIn('id', $request->ids)->delete();
        return response()->json(['deleted' => true]);
    }

}
