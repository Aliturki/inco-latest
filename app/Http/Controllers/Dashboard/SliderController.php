<?php

namespace App\Http\Controllers\Dashboard;

use App\Slider;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'model' => Slider::FilterPaginateOrder()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'form' => Slider::form()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 100 حرف',
            'title.min'=>'لا يمكن ان يحتوى العنوان على إقل من 6 احرف',
            'description.max'=>'لا يمكن ان يحتوى الوصف على أكثر من 500 حرف',
            'description.min'=>'لا يمكن ان يحتوى الوصف على إقل من 60 احرف',
            'image.image'=>'يقبل صور فقط',
            'image.required'=>'الصوره مطلوبه',
            'description.required'=>'الوصف مطلوب',
            'url.url'=>'أدخل رابط صحيح'
        ];

        Validator::make($request->all(),[
            'title' => 'required|min:6|max:100',
            'image' => 'required|image',
            'description' => 'required|min:60|max:500',
            'ur'=>'url|nullable'

        ],$custom_ms)->validate();

        $file = $request->file('image');
        $filename = Carbon::now()->toDateString() . '_' . str_random(10) . '.' . $file->extension();
        $file->move(public_path() . '/uploads/', $filename);

        $slider = new Slider($request->all());
        $slider->is_active = inco_activation($request->input('is_active'));
        $slider->image = $filename;
        $slider->save();
        return response()->json(['success' => true]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    public function edit(Slider $slider)
    {

        return response()->json([
            'form' => $slider
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 100 حرف',
            'title.min'=>'لا يمكن ان يحتوى العنوان على إقل من 6 احرف',
            'description.max'=>'لا يمكن ان يحتوى الوصف على أكثر من 500 حرف',
            'description.min'=>'لا يمكن ان يحتوى الوصف على إقل من 60 احرف',
            'description.required'=>'الوصف مطلوب'
        ];

        Validator::make($request->all(),[
            'title' => 'required|min:6|max:100',
            'image' => 'image',
            'description' => 'required|min:60|max:500',
            'ur'=>'url'

        ],$custom_ms)->validate();

        $slider = Slider::findOrFail($id);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $file = $request->file('image');
            $filename = Carbon::now()->toDateString() . '_' . str_random(10) . '.' . $file->extension();
            $file->move(public_path() . '/uploads/', $filename);
            $slider->image = $filename;
        }

        $slider->title = $request->title;
        $slider->url = $request->url;
        $slider->description = $request->description;
        $slider->is_active =inco_activation($request->input('is_active'));
        $slider->update();
        return response()->json(['success' => true]);
    }

    public function activation($id)
    {

        $slider = Slider::findOrFail($id);

        $slider->is_active = !$slider->is_active;
        $slider->update();

        $msg = $slider->is_active ? 'تم التفيعل' : 'تم إلغاء التفعيل';
        return response()->json([
            'success' => true,
            'msg' => $msg
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        File::delete(public_path('uploads/' . $slider->image));
        $slider->delete();

        return response()->json([
            'deleted' => true
        ]);
    }

    public function delete(Request $request)
    {
//        return $request->all();
        Slider::whereIn('id', $request->ids)->delete();
        return response()->json(['deleted' => true]);
    }
}
