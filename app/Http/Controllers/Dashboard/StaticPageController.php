<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\StaticPage;
use Validator;
class StaticPageController extends Controller
{

    public function index()
    {

        return response()
            ->json([
                'model' => StaticPage::filterPaginateOrder()
            ]);
    }


    public function create()
    {
        return response()
            ->json([
                'form' => StaticPage::form()
            ]);
    }


    public function store(Request $request)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 150 حرف',
            'slug.unique'=>'الرابط موجود مسبقاً',
            'content.required'=>'المحتوي مطلوب',
        ];

        Validator::make($request->all(),[
            'title' => 'required|max:150',
            'slug' => 'nullable|max:150|unique:static_pages',
            'content' => 'required',

        ],$custom_ms)->validate();

        $page = new StaticPage($request->all());
        $page->content =  inco_felter_content('content');
        $page->is_active = inco_activation($request->is_active);
        if ($request->has('slug') && $request->slug != null) {
            $page->slug = str_slug($request->slug, '-');
        } else {
            $page->slug = str_slug($request->title, '-');
        }
        $page->save();
        return response()->json(['success' => true]);
    }


    public function show($id)
    {
        $page = StaticPage::findOrFail($id);

        return response()->json(['form' => $page]);
    }


    public function edit($id)
    {
        $page = StaticPage::findOrFail($id);

        return response()->json(['form' => $page]);
    }


    public function update(Request $request, $id)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 150 حرف',
            'slug.unique'=>'الرابط موجود مسبقاً',
            'content.required'=>'المحتوي مطلوب',
        ];

        Validator::make($request->all(),[
            'title' => 'required|max:150',
            'slug' => 'nullable|max:150|unique:static_pages,slug,'.$request->id,
            'content' => 'required',

        ],$custom_ms)->validate();

        $page = StaticPage::findOrFail($id);


        $page->title = $request->title;
        $page->is_active = inco_activation($request->is_active);
        $page->content = inco_felter_content('content');
        $page->update();

        return response()->json(['success' => true]);
    }




    public function delete(Request $request)
    {
        StaticPage::whereIn('id', $request->ids)->delete();
        return response()->json(['deleted' => true]);
    }

}
