<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Article;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Validator;

class ArticleController extends Controller
{

    public function index()
    {
        return response()->json([
            'model'=>Article::filterPaginateOrder()
        ]);
    }

    public function create()
    {
        return response()->json([
            'form'=>Article::form()
        ]);
    }


    public function store(Request $request)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 150 حرف',
            'image.required'=>'الصوره مطلوبه',
            'image.image'=>'يقبل صور فقط',
            'author.required'=>'أسم الكاتب مطلوب',
            'content.required'=>'المحتوي مطلوب',
            'small_content.required'=>'المحتوي مطلوب',

        ];

        Validator::make($request->all(),[
            'title'=>'required|max:150',
            'content'=>'required',
            'image'=>'required|image',
            'author'=>'required',
            'small_content'=>'required'
        ],$custom_ms)->validate();

        $file = $request->file('image');
        $filename = Carbon::now()->toDateString() . '_' . str_random(10) . '.' . $file->extension();
        $file->move(public_path() . '/uploads/', $filename);

        $article = new Article($request->all());
        $article->content = inco_felter_content('content');
        $article->is_active =  inco_activation($request->input('is_active'));
        $article->image = $filename;
        $article->saveOrFail();
        return response()->json(['success'=>true]);


    }


    public function show($article)
    {
        $article = Article::findOrFail($article);

        return response()->json(['article'=>$article]);
    }


    public function edit($article)
    {
        $article = Article::findOrFail($article);

        return response()->json(['form'=>$article]);
    }

    public function update(Request $request,$article)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 150 حرف',
            'image.image'=>'يقبل صور فقط',
            'author.required'=>'أسم الكاتب مطلوب',
            'content.required'=>'المحتوي مطلوب',
            'small_content.required'=>'المحتوي مطلوب',

        ];

        Validator::make($request->all(),[
            'title'=>'required|max:150',
            'content'=>'required',
            'image'=>'image',
            'author'=>'required',
            'small_content'=>'required',

        ],$custom_ms)->validate();

        $article = Article::findOrFail($article);

        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->small_content = $request->input('small_content');
        $article->is_active =  inco_activation($request->input('is_active'));

        if($request->has('author')){
            $article->author = $request->input('author');
        }

        if($request->hasFile('image') && $request->file('image')->isValid()){
            $image = $request->file('image');
            $file_name = str_random(10).Carbon::now()->toDateString().'.'.$request->file('image')->getExtension();
            $image->move(public_path('uploads/'),$file_name);


            File::delete(public_path('uploads/'.$article->image));

        }

        $article->saveOrFail();
        return response()->json(['success'=>true]);

    }

    public function destroy(Request $request){

        $articles = $request->input('articles');
        $ids = [];

        foreach($articles as $article ){
            $ids[] = $article->id;
        }

        Article::whereIn('id',$ids)->delete();

        return response()->json(['deleted'=>true]);


    }

    public function activation($id)
    {

        $article = Article::findOrFail($id);

        $article->is_active = !$article->is_active;
        $article->update();

        $msg = $article->is_active ? 'تم التفيعل' : 'تم إلغاء التفعيل';
        return response()->json([
            'success' => true,
            'msg' => $msg
        ]);
    }


}
