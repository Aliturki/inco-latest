<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\Request;
use Validator;
class ServiceController extends Controller
{

    public function index()
    {
        return response()->json([
            'model'=>Service::filterPaginateOrder()
        ]);
    }


    public function create()
    {
        return response()->json([
            'form'=>Service::form()
        ]);
    }


    public function store(Request $request)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 150 حرف',
            'description.required'=>'الوصف مطلوب',
            'content.required'=>'المحتوي مطلوب',
        ];

        Validator::make($request->all(),[
            'title'=>'required|max:150',
            'description'=>'required',
            'content'=>'required',
        ],$custom_ms)->validate();

        $service = new Service($request->all());
        $service->content =  inco_felter_content('content');
        $service->is_active = inco_activation($request->input('is_active'));
        $service->saveOrFail();
        return response()->json(['success'=>true]);

    }

    public function show($id)
    {
        $service = Service::findOrFail($id);

        return response()->json(['service'=>$service]);
    }


    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return response()->json([
            'form'=>$service
        ]);
    }


    public function update(Request $request, $id)
    {
        $custom_ms = [
            'title.required'=>'العنوان مطلوب',
            'title.max'=>'لا يمكن ان يحتوى العنوان على أكثر من 150 حرف',
            'description.required'=>'الوصف مطلوب',
            'content.required'=>'المحتوي مطلوب',
        ];

        Validator::make($request->all(),[
            'title'=>'required|max:150',
            'description'=>'required',
            'content'=>'required',
        ],$custom_ms)->validate();

        $service = Service::findOrFail($id);

        $service->title = $request->title;
        $service->description = $request->description;
        $service->content =  inco_felter_content('content');
        $service->is_active = inco_activation($request->input('is_active'));
        $service->update();
        return response()->json(['success'=>true]);
    }


    public function delete(Request $request)
    {
//        return $request->all();
        Service::whereIn('id', $request->ids)->delete();
        return response()->json(['deleted' => true]);
    }
}
