<?php

namespace App\Http\Controllers\Dashboard;

use App\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscribeController extends Controller
{
    public function index()
    {
        return response()
            ->json([
                'model' => Subscribe::filterPaginateOrder()
            ]);
    }
}
