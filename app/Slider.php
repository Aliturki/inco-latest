<?php

namespace App;
use App\BaseModel;
class Slider extends BaseModel
{


    protected $guarded = [];
    protected $filter = [
        'id', 'title', 'is_active','created_at'
    ];


    public static function form()
    {
        return [
            'title' => '',
            'image' => '',
            'description' => '',
            'url' => '',
            'is_active' => true
        ];
    }

    public function getIsActiveAttribute($value)
    {
        return $value ? true : false;
    }

}