<?php

namespace App;


use Carbon\Carbon;

class Subscribe extends BaseModel
{

    protected  $fillable = ['email','ip'];
    protected $filter = [
        'id','created_at','ip'
    ];



    public function getCreatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->toDateString();
    }


}
