<?php

namespace App;


use Carbon\Carbon;

class Testimonial extends BaseModel
{

    protected $guarded = [];



    protected $filter = [
        'id','is_active','created_at'
    ];


    public static function form(){
        return [
            'speaker'=>'',
            'content'=>'',
            'is_active'=>true
        ];
    }

    public function getCreatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->toDateString();
    }


    public function getIsActiveAttribute($value)
    {
        return $value ? true : false;
    }


}
