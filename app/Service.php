<?php

namespace App;

use App\BaseModel;
use Carbon\Carbon;

class Service extends BaseModel
{
    protected $guarded = [];

    protected $filter = [
        'id','title','is_active','created_at'
    ];


    public static function form(){
        return [
            'title'=>'',
            'description'=>'',
            'content'=>'',
            'is_active'=>true
        ];
    }


    public function getContentAttribute($val){
        return stripcslashes($val);
    }


    public function getCreatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->toDateString();
    }


    public function getIsActiveAttribute($value)
    {
        return $value ? true : false;
    }




}
