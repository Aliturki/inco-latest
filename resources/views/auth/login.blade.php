<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Page Description">
    <meta name="author" content="Ali Turki">
    <title>Page Title</title>

    <!-- Bootstrap -->
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">
    <br>
    <img src="{{asset('updated.png')}}" alt="{{getSetting()->site_name}}" class="img-responsive center-block" width="100">
    <h4 class="text-center">{{getSetting()->site_name}}</h4>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div id="login">
                <form action="{{url('/login')}}" method="post" role="form">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <legend>تسجيل الدخول</legend>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">البريد الإلكتروني</label>

                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">كلمة المرور</label>
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                    </div>



                    <div class="form-group pull-left">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="remember"/>
                        <label for="remember">ذكرني</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">تسجيل الدخول</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    body{
        background-color: #5DBB63;
        color: #5DBB63;
    }
    #login{
        background-color: #fbfafa;
        margin: 50px auto;
        padding: 10px;
        /*box-shadow: 1px 3px 6px #ddd;*/
    }
    .form-control{
        background: #fbfbfb;
        box-shadow: none;
        border-color: #e4e4e4;
        height: 45px;
    }
    h4{
        color: #fff;
    }
    legend{
        padding: 10px 0;
        color: #5DBB63;

    }

</style>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>

