@extends('website.layouts.master')


@section('content')

    <!-- HERO SLIDER -->
    @include('website.inc.home.slider')

{{--Welcome Arrea--}}
    @include('website.inc.home.welcome')

    <!--/*Our Services*/-->
    @include('website.inc.home.service')
    <!--/*end Our Services*/-->


    <!-- CONTENT START -->
    <section id="home-content">

        <div id="home-intro" class="mission-content" class="padding35">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInDown">
                        <h3 class="text-center">مميزاتنا</h3>
                        <br>
                        {{strip_tags(str_limit($features->content,500,'...'))}}
                    </div>
                    <div class="col-md-6 wow fadeInDown">
                        <h3 class="text-center">رؤيتنا</h3>
                        <br>
                        {{strip_tags(str_limit($roeytna->content,500,'...'))}}

                    </div>
                </div>
            </div>
        </div>

        <div id="testimonials" class="padding35">
            <div class="container">
                <div class="row wow fadeIn">
                    @include('website.inc.home.article')
                    @include('website.inc.home.testimonial')
                </div>
            </div>
        </div>


    </section>

    <style>
        .mission-content{
            line-height:1.8;
            padding:50px 0
        }
        #testimonials{
            background-color:#f3f3f3
        }
    </style>
    <!-- CONTENT END -->
@endsection