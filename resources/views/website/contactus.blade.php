@extends('website.layouts.master')


@section('content')
        <!-- PAGE TITLE START -->
        <section id="title" class="container-fluid wow fadeInDown">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <h1>راسلنا</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- PAGE TITLE END -->


        <!-- CONTENT START -->
        <section id="content">

            <div class="container">

                <div class="row">
                    <div class="col-md-8">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>شكراً لك</strong> {{session('success')}} ...
                            </div>
                        @endif

                        <div class="padding25">
                            <h4>راسلنا</h4>
                        </div>

                        <form method="post" action="{{route('contact::store')}}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="form-group col-md-4 {{$errors->has('name') ? 'has-error':''}}">
                                    <input type="text" name="name" value="{{old('name')}}" class="form-control input-lg" placeholder="إسمك *" required>
                                    @if($errors->has('name'))
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif

                                </div>
                                <div class="form-group col-md-4 {{$errors->has('email') ? 'has-error':''}}">
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control input-lg" placeholder="البريد الإلكتروني*" required>
                                    @if($errors->has('email'))
                                        <span class="help-block">{{$errors->first('email')}}</span>
                                    @endif

                                </div>
                                <div class="form-group col-md-4 {{$errors->has('phone') ? 'has-error':''}}">
                                    <input type="tel"  name="phone"  value="{{old('phone')}}"  class="form-control input-lg" placeholder="رقم الهاتف">
                                    @if($errors->has('phone'))
                                        <span class="help-block">{{$errors->first('phone')}}</span>
                                    @endif

                                </div>
                                <div class="form-group col-md-12 {{$errors->has('subject') ? 'has-error':''}}">
                                    <input type="text" name="subject" value="{{old('subject')}}"  class="form-control input-lg" placeholder="موضوع الرساله">
                                    @if($errors->has('subject'))
                                        <span class="help-block">{{$errors->first('subject')}}</span>
                                    @endif

                                </div>

                                <div class="form-group col-xs-12 {{$errors->has('message') ? 'has-error':''}}">
                                    <textarea class="form-control" name="message" rows="11" placeholder="الرساله*" >{{old('message')}}</textarea>
                                    @if($errors->has('message'))
                                        <span class="help-block">{{$errors->first('message')}}</span>
                                    @endif
                                </div>

                                <div class="form-group col-xs-12 text-right">
                                    <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-send fa-fw"></i> إرسال</button>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="col-md-4">


                        <div class="padding15">
                            <h4>بيانات المكتب</h4>
                        </div>
                        @if(getSetting()->address)
                            <p><i class="fa fa-map-pin fa-fw"></i> {{getSetting()->address}}</p>
                        @endif
                        @if(getSetting()->phone)
                            <p><i class="fa fa-phone fa-fw"></i> {{getSetting()->phone}}</p>
                        @endif
                        @if(getSetting()->email)
                            <p><i class="fa fa-envelope fa-fw"></i> {{getSetting()->email}}</p>
                        @endif


                    </div>

                </div>

            </div>



        </section>
        <!-- CONTENT END -->

        <section id="full-map" class="container-fluid map-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d110504.96482634991!2d31.305970548931377!3d30.057503204869718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145845dc8fe869d5%3A0x3e69c6572ccc5f4f!2z2YXYs9ix2K0g2KfZhNmH2LHZhQ!5e0!3m2!1sar!2seg!4v1491756081346" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>        </section>

    @endsection