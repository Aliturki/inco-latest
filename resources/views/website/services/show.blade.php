@extends('website.layouts.master')

@section('content')
    <!-- PAGE TITLE START -->
    <section id="title" class="container-fluid wow fadeInDown">
        <div class="container">
            <h1>الخدمات
                <small>{{$service->title}}</small>
            </h1>
        </div>
    </section>
<section id="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="blog-item-inner">
                    {!! $service->content !!}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection