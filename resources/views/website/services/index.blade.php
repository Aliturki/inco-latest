@extends('website.layouts.master)


@section('content')
    <!-- PAGE TITLE START -->
    <section id="title" class="container-fluid wow fadeInDown">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <h1>Blog <small>Single</small></h1>
                </div>
                <div class="col-xs-6 text-right breadcrumbs">
                    <ul class="list-inline list-unstyled">
                        <li><a href="index-2.html">Home</a></li>
                        <li>/</li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- PAGE TITLE END -->


    <!-- CONTENT START -->
    <section id="content">

        <section id="blog-single" class="container">
            <div class="row">

                <!-- left column start -->
                <div class="col-md-9">

                    <div class="row">

                        <div class="col-md-12 wow fadeIn">
                            <div class="blog-item">
                                <div class="row">


                                    <div class="col-md-12">
                                        <img alt="" class="img-responsive" src="images/blog/blog5.jpg">
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row blog-meta">
                                            <div class="col-xs-4 text-left"><i class="fa fa-user fa-fw"></i> August, 4th, 2016</div>
                                            <div class="col-xs-4 text-center"><i class="fa fa-user fa-fw"></i> John Smith</div>
                                            <div class="col-xs-4 text-right"><i class="fa fa-folder-open fa-fw"></i> Wesite Templates</div>
                                        </div>

                                        <div class="blog-item-inner">
                                            <h2>Best website template</h2>
                                            <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.</p>

                                            <ul>
                                                <li>At vero eos et accusamus et iusto odio dignissimos ducimus qui</li>
                                                <li>Itaque earum rerum hic tenetur a sapiente delectus</li>
                                                <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque</li>
                                            </ul>

                                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>

                                            <blockquote>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                                <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                                            </blockquote>

                                        </div>


                                        <div class="comments">
                                            <div class="page-header">
                                                <h2><small class="pull-right">3 comments</small> Comments </h2>
                                            </div>

                                            <div class="comments-list">

                                                <div class="media">
                                                    <p class="pull-right"><small>2 days ago</small></p>
                                                    <a class="media-left" href="#">
                                                        <img alt="" class="img-circle avatar" src="images/team/thumb8_40.jpg">
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading user_name">John Smith <small>says</small></h4>
                                                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
                                                    </div>
                                                </div>

                                                <div class="media">
                                                    <p class="pull-right"><small>4 days ago</small></p>
                                                    <a class="media-left" href="#">
                                                        <img alt="" class="img-circle avatar" src="images/team/thumb1_40.jpg">
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading user_name">Sarah Black <small>says</small></h4>
                                                        Nunc risus ex, tempus quis purus ac, tempor consequat ex. Vivamus sem magna, maximus at est id, maximus aliquet nunc. Suspendisse lacinia velit a eros porttitor, in interdum ante faucibus.
                                                    </div>
                                                </div>

                                                <div class="media">
                                                    <p class="pull-right"><small>5 days ago</small></p>
                                                    <a class="media-left" href="#">
                                                        <img alt="" class="img-circle avatar" src="images/team/thumb4_40.jpg">
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading user_name">Jane Doe <small>says</small></h4>
                                                        Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="page-header">
                                                <h2>Leave a Reply</h2>
                                            </div>

                                            <p class="text-muted">You must be logged in to post a comment.</p>

                                            <form>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <input type="text" class="form-control input-lg" placeholder="Name*" required>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <input type="email" class="form-control input-lg" placeholder="Email*" required>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <input type="text" class="form-control input-lg" placeholder="Website">
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <textarea class="form-control" rows="6" placeholder="Message*" required></textarea>
                                                    </div>

                                                    <div class="form-group col-xs-12 text-right">
                                                        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-send fa-fw"></i> POST COMMENT</button>
                                                    </div>
                                                </div>


                                            </form>



                                        </div>





                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>



                </div>
                <!-- left column end -->


                <!-- sidebar start -->
                <div id="sidebar" class="col-md-3">

                    <div class="widget">
                        <form class="form-inline">
                            <div class="input-group">
                                <input type="text" class="form-control input-lg" placeholder="Search...">
                                <span class="input-group-btn">
                                        <button class="btn btn-default btn-lg" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                            </div>

                        </form>
                    </div>

                    <div class="widget">
                        <h4>ABOUT</h4>
                        <p>Nulla eleifend, sapien eget porttitor maximus, nisl ante convallis dolor, nec consequat felis ex a ex. Etiam vestibulum enim euismod dui vestibulum, vitae fringilla nibh consectetur. Integer at volutpat augue.</p>
                    </div>

                    <div class="widget">
                        <h4>CATEGORIES</h4>
                        <ul class="list-unstyled link-list">
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Site Themes (5)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Business (2)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Design (3)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Social Network (6)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Team Work (1)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Uncategorized (5)</a></li>
                        </ul>
                    </div>

                    <div class="widget">
                        <h4>ARCHIVES</h4>
                        <ul class="list-unstyled link-list">
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">September 2016 (5)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">August 2016 (2)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">July 2016 (3)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">June 2016 (6)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">May 2016 (1)</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">April 2016 (5)</a></li>
                        </ul>
                    </div>

                    <div class="widget">
                        <h4>META</h4>
                        <ul class="list-unstyled link-list">
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Site Admin</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Log out</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Entries RSS</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">Comments RSS</a></li>
                            <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="#">WordPress.org</a></li>

                        </ul>
                    </div>


                </div>
                <!-- sidebar end -->

            </div>
        </section>



    </section>
    <!-- CONTENT END -->

@endsection