@extends('website.layouts.master')


@section('content')

    <!-- PAGE TITLE START -->
    <section id="title" class="container-fluid wow fadeInDown">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <h1>المدونه </h1>
                </div>
                <div class="col-xs-6 text-left breadcrumbs">
                    <ul class="list-inline list-unstyled">
                        <li><a href="url('/')">الرئيسيه</a></li>
                        <li>/</li>
                        <li>المدونه</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- PAGE TITLE END -->


    <!-- CONTENT START -->
    <section id="content">

        <section id="blog-grid" class="container">

            <div class="row">
                @foreach($articles as $article)
                    <div class="col-md-4 wow fadeInUp">
                        <div class="text-center blog-item">
                            <a href="{{route('article::show',$article->id)}}">
                                <img alt="" class="img-responsive" src="{{asset('/uploads/'.$article->image)}}">
                            </a>

                            <div class="blog-item-inner">
                                <h3><a href="#">{{$article->title}}</a></h3>
                                <p>{{$article->small_content}}</p>
                                <a href="{{route('article::show',$article->id)}}" class="btn btn-sm btn-default">إقراء المزيد</a>
                            </div>

                            <div class="row blog-meta">
                                <div class="col-xs-6 text-left"><i class="fa fa-clock-o fa-fw"></i> {{$article->created_at->toDateString()}}</div>
                                <div class="col-xs-6 text-right"><i class="fa fa-user fa-fw"></i> {{$article->author}}</div>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>

            <div id="pagination" class="row text-center">
               {!! $articles->links() !!}
            </div>

        </section>



    </section>
    <!-- CONTENT END -->
@endsection