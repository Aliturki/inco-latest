@extends('website.layouts.master')


@section('content')

    <!-- PAGE TITLE START -->
    <section id="title" class="container-fluid wow fadeInDown">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <h1>المدونه
                        <small>{{$article->title}}</small>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- PAGE TITLE END -->
    <!-- CONTENT START -->
    <section id="content">

        <section id="blog-single" class="container">
            <div class="row">

                <!-- left column start -->
                <div class="col-md-9">

                    <div class="row">

                        <div class="col-md-12 wow fadeIn">
                            <div class="blog-item">
                                <div class="row">


                                    <div class="col-md-12">
                                        <img alt="" class="img-responsive" src="{{asset('/uploads/'.$article->image)}}">
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row blog-meta">
                                            <div class="col-xs-4 text-right"><i
                                                        class="fa fa-calendar fa-fw"></i> {{$article->created_at->toDateString()}}
                                            </div>
                                            <div class="col-xs-4 text-center"><i
                                                        class="fa fa-user fa-fw"></i> {{$article->author}}</div>
                                        </div>

                                        <div class="blog-item-inner">
                                            <h2>{{$article->title}}</h2>
                                            {!! $article->content !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
                <!-- left column end -->


                <!-- sidebar start -->
                <div id="sidebar" class="col-md-3">


                    <div class="widget">
                        <h4>أحدث المقالات</h4>
                        <ul class="list-unstyled link-list">
                            @foreach($titles as $title)
                                <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a href="{{route('article::show',$title->id)}}">{{$title->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>


                </div>
                <!-- sidebar end -->

            </div>
        </section>


    </section>
    <!-- CONTENT END -->


@endsection