<!-- FOOTER START -->
<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <img src="{{asset('updated.png')}}" alt="" width="80" class="center-block">
                <br>
                <p>{{str_limit(getSetting()->description,100,'....')}}</p>
                <br>
                <ul class="list-unstyled list-inline social-icons">
                    @if(getSetting()->facebook)
                        <li><a href="{{getSetting()->facebook}}" target="_blank" class="facebook"><i class="fa fa-facebook facebook"></i></a></li>
                    @endif
                </ul>
            </div>

            <div class="col-md-4  col-md-offset-1">
                <h4>اتصل بنا</h4>
                @if(getSetting()->address)
                    <p><i class="fa fa-map-pin fa-fw"></i> {{getSetting()->address}}</p>
                @endif
                @if(getSetting()->phone)
                    <p><i class="fa fa-phone fa-fw"></i> {{getSetting()->phone}}</p>
                @endif
                @if(getSetting()->email)
                    <p><i class="fa fa-envelope fa-fw"></i> {{getSetting()->email}}</p>
                @endif
            </div>
        </div>

        <div class="row">
            <hr>
            <div class="col-md-12 text-center bottom">
                <p class="text-capitalize">powered by. <a href="https://www.facebook.com/aliturki2020" target="_blank" data-toggle="tooltip" title="aliturki2020@gmail.com">ali turki</a></p>

            </div>
        </div>
    </div>
</section>
<!-- FOOTER END -->


</div>
<!-- WRAPPER END -->

<!-- back to top button -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/wow.min.js')}}"></script>

</body>
</html>