<div class="col-md-6">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-uppercase text-bold text-center">أخر المقالات</h4>
            <div class="space20"></div>
        </div>

        <div class="col-md-12">
            <!-- carousel start -->
            <div id="blog-carousel-home" class="owl-carousel text-right" dir="ltr">
                @foreach($articles as $article)
                    <div>
                        <div class="blog-item">
                            <div class="row">
                                <div class="col-md-5">
                                    <a href="#">
                                        <img alt="" class="img-responsive" src="{{asset('/uploads/'.$article->image)}}">
                                    </a>
                                </div>

                                <div class="col-md-7">
                                    <div class="blog-item-inner">
                                        <h4><a href="{{route('article::show',$article->id)}}" class="text-bold">{{$article->title}}</a></h4>
                                        <p>{{str_limit($article->small_content,250,'...')}}</p>
                                    </div>
                                    <div class="row blog-meta">
                                        <div class="col-xs-6"><i class="fa fa-calendar fa-fw"></i> {{$article->created_at->toDateString()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- carousel end -->
        </div>
    </div>
</div>