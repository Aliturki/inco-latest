<div class="col-md-6">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-uppercase text-bold text-center">قالوا عنا</h4>
            <div class="space20"></div>
        </div>

        <div class="col-md-12">
            <!-- carousel start -->
            <div id="testimonials-carousel-home" class="owl-carousel" dir="ltr">
                @foreach($testimonial as $test)
                    <div class="testimonial">
                        <blockquote>
                            <p>{{str_limit($test->content,360)}}</p>
                            <small>{{$test->speaker}} </small>
                        </blockquote>
                    </div>

                @endforeach
            </div>
            <!-- carousel end -->
        </div>
    </div>
</div>