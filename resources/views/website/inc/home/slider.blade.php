<section class="cd-hero">
    <ul class="list-unstyled cd-hero-slider autoplay">

        @foreach($sliders as $slider)
            <li class="selected" style="background-image:url({{asset('uploads/'.$slider->image)}});">
                <div class="container">
                    <h1>{{$slider->title}}</h1>
                    <h2>{{str_limit($slider->description,100,'...')}}</h2>
                    @if($slider->url)
                        <a href="{{$slider->url}}" target="_blank" class="btn btn-primary btn-lg">المزيد</a>
                    @endif
                </div>
            </li>
        @endforeach

    </ul>


    <div class="arrow-nav">
        <div class="prev"><a class="hero-prev" href="#"><i class="fa fa-angle-left"></i></a></div>
        <div class="next"><a class="hero-next" href="#"><i class="fa fa-angle-right"></i></a></div>
    </div>

</section>
