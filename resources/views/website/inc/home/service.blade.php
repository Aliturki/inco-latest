<section id="our-services">
    <div class="container text-center">

        <h1 class="text-center">خدماتنا</h1>

        <div id="service-c" class="owl-carousel" dir="ltr">
            @foreach($services as $serv)
            <div class="item wow fadeIn">
                <div class="serv">
                    <h3>{{$serv->title}}</h3>
                    <p>{{str_limit($serv->description,150,'...')}}</p>
                    <a href="{{route('service::show',$serv->id)}}" class="btn btn-primary">المزيد</a>
                    <br>
                    <br>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>