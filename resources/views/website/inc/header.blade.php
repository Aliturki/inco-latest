<!doctype html>
<html dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="{{getSetting()->description}}">
    <meta name="keywords" content="{{getSetting()->keywords}}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{getSetting()->site_name}}</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>

<!-- WRAPPER START -->
<div id="wrapper">
    <!-- HEADER START -->
    <section id="header">
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="{{asset('updated.png')}}" style="max-width: 100%;max-height: 100%;">
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{url('/')}}">الرئيسيه </a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">خدمات المكتب</a>
                            <ul class="dropdown-menu">
                                @foreach(getServices() as $serv)
                                    <li><a href="{{route('service::show',$serv->id)}}">{{$serv->title}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li><a href="{{route('blog::index')}}">المدونه </a></li>
                        <li><a href="{{route('page::show',getAbout()->slug)}}">من نحن </a></li>
                        <li><a href="{{route('contact::show')}}">اتصل بنا </a></li>
                    </ul>


                </div><!--/.nav-collapse -->
            </div>
        </nav>
    </section>
    <!-- HEADER END -->