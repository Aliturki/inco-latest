window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
require('./script');

window.Vue = require('vue');
window.axios = require('axios');
// Vue Material
var VueMaterial = require('vue-material');
Vue.use(VueMaterial);

//  NProgress
window.NProgress = require('nprogress');
NProgress.configure({showSpinner: false});
// Toastr
window.toastr = require('toastr');
toastr.options.rtl = true;
// Axios Configurations

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min')
// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')
require('froala-editor/js/languages/ar')

// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala);

require('./helpers/axios-config')
// Modules Styles
require('nprogress/nprogress.css');
