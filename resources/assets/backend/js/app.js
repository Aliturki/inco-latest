require('./bootstrap');
import router from './router'
import store from './store'
import App from './App.vue';
import cardLoading from './components/zkloading.vue';

Vue.component('zk-card-loading',cardLoading);
const app = new Vue({
    components:{
        'app':App
    },
    router,
    store
}).$mount('#app');
