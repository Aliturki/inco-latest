export function toMultipartedForm(form){

    if(typeof form.image === 'string'){
        const temp = JSON.parse(JSON.stringify(form));
        delete temp.image;
        return temp
    }else {
        return objectToformData(form)
    }


}


export function objectToformData(obj,form,namespace){

    let fd = form || new FormData();

    let formKey;

    for(var property in obj){


        if(obj.hasOwnProperty(property)){

            if(namespace){
                formKey = namespace + '['+property+']';
            }else{
                formKey = property;

            }

            if(obj[property] instanceof Array){
                for (var i = 0; i < obj[property].length;i++){
                    objectToformData(obj[property][i],fd,`${property}[${i}]`);
                }

            }else if(typeof obj[property] === 'object' && !(obj[property] instanceof File)){
                objectToformData(obj[property],fd,property)
            }else{

                fd.append(formKey,obj[property])
            }


        }

    }


    return fd

}