export const options = {
    plugins: 'image code',
    toolbar: 'undo redo | link image | code',
    automatic_uploads: false,
    images_reuse_filename: true,

    images_upload_handler: function (blobInfo, success, failure) {
        var formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        axios.post('/dashboard/api/upload-image', formData).then(res => {
            console.log(res)
        }, err => {
            console.log(res)
        });
    },
    file_picker_types: 'image',
    file_picker_callback: function (cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        input.onchange = function () {
            var file = this.files[0];
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var blobInfo = blobCache.create(id, file);
            blobCache.add(blobInfo);
            cb(blobInfo.blobUri(), {title: file.name});
        };

        input.click();
    }
}