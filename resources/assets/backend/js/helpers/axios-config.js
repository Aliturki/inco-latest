axios.defaults.baseURL = '/dashboard/api';
window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

axios.interceptors.request.use(function (config) {
    NProgress.start();

    return config;
}, function (error) {
    NProgress.done();
    // Do something with request error
    return Promise.reject(error);
});
// or require
axios.interceptors.response.use(function (response) {
    NProgress.done();
    return response;
}, function (error) {
    // Do something with response error
    NProgress.done();
    return Promise.reject(error);
});
