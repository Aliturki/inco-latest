export const FroalaConfig = {
    events: {
        'froalaEditor.image.removed': function (e, editor, $img) {
            var img = $img.attr('src');
            console.log(img);

            axios.post('/delete-image', {src: img}).then(res => {
                console.log(res);
            }, err => {
                console.log(err);
            })
        }
    },
    language: 'ar',
    direction: 'rtl',
    requestHeaders: {
        'X-CSRF-TOKEN': window.Laravel.csrfToken,
        'X-Requested-With': 'XMLHttpRequest'
    },
    imageUploadURL: '/dashboard/api/upload-image',
    imageAllowedTypes: ['jpeg', 'jpg', 'png'],
    //EDitor Config
    toolbarButtons:['bold', 'italic', 'underline','fontFamily', 'fontSize', '|','color',  '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', '-', 'insertLink', 'insertImage','insertTable', 'undo', 'redo', 'clearFormatting', 'selectAll']

};