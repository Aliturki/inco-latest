const state = {
    user:{},
    breads:{},
    loading:false,
    messageCount:null
};



const mutations = {
  initUser(state,user){
      state.user = user
  },
  addBreads(state,breads){
      state.breads = breads
  },
  getMessageCount(state){
      axios.get('/message/count').then(function(res){
          state.messageCount = res.data.msg_cont
      })
  },
    loader(state,status){
      state.loading = status
    }
};


export default {
    state,mutations
}