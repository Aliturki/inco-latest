import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import homeDashboard from './views/home/dashboard.vue';
import homeSiteSetting from './views/home/siteSetting.vue';
import userEdit from './views/user/edit.vue';
import sliderIndex from './views/slider/index.vue';
import sliderCreate from './views/slider/create.vue';
import ServiceCreate from './views/service/create.vue';
import ServiceIndex from './views/service/index.vue';
import ServiceShow from './views/service/show.vue';
import sliderShow from './views/slider/show.vue';
// static pages
import pageIndex from './views/page/index.vue';
import pageCreate from './views/page/create.vue';
import pageShow from './views/page/show.vue';
// article
import articleIndex from './views/article/index.vue';
import articleCreate from './views/article/create.vue';
import articleShow from './views/article/show.vue';
// Mailing
import mailingIndex from './views/mailing/index.vue';
import mailingCreate from './views/mailing/create.vue';
import mailingShow from './views/mailing/show.vue';
// Testimonial
import testimonialIndex from './views/testimonial/index.vue';
import testimonialCreate from './views/testimonial/create.vue';
import testimonialShow from './views/testimonial/show.vue';
// Contact
import contactIndex from './views/contact/index.vue';
//Test View
import TestView from './views/test.vue';

const routes = [
    {path:'/dashboard/',name:'dashboard',component:homeDashboard,meta:{title:'الرئيسيه'}},
    {path:'/dashboard/site_setting',name:'siteSetting',component:homeSiteSetting,meta:{title:'إعدادات الموقع'}},
    {path:'/dashboard/user/edit',name:'userEdit',component:userEdit,meta:{title:'تعديل بياناتي'}},
    {path:'/dashboard/slider/create',name:'sliderCreate',component:sliderCreate,meta:{title:'الصور المتحركة - إضافة صوره حديده'}},
    {path:'/dashboard/slider',name:'sliderIndex',component:sliderIndex,meta:{title:'الصور المتحركه'}},
    {path:'/dashboard/slider/:id',name:'sliderShow',component:sliderShow,meta:{title:'الصور المتحركة - عرض'}},
    {path:'/dashboard/service/create',name:'serviceCreate',component:ServiceCreate,meta:{title:'الخدمات - إضافة خدمه جديده'}},
    {path:'/dashboard/service',name:'serviceIndex',component:ServiceIndex,meta:{title:'الخدمات'}},
    {path:'/dashboard/service/:id/edit',name:'serviceShow',component:ServiceShow,meta:{title:'الخدمات - عرض'}},
    // static pages
    {path:'/dashboard/page',name:'pageIndex',component:pageIndex,meta:{title:'الصفحات'}},
    {path:'/dashboard/page/create',name:'pageCreate',component:pageCreate,meta:{title:'الصفحات - إضافة صفحة جديده'}},
    {path:'/dashboard/page/:id/edit',name:'pageShow',component:pageShow,meta:{title:'الصفحات - تعديل الصفحة'}},
    //article
    {path:'/dashboard/article',name:'articleIndex',component:articleIndex,meta:{title:'المقالات'}},
    {path:'/dashboard/article/create',name:'articleCreate',component:articleCreate,meta:{title:'المقالات - إضافة مقاله جديده'}},
    {path:'/dashboard/article/:id/edit',name:'articleShow',component:articleShow,meta:{title:'المقالات - تعديل المقاله'}},
    //testimonial
    {path:'/dashboard/testimonial',name:'testimonialIndex',component:testimonialIndex,meta:{title:'قالوا عنا'}},
    {path:'/dashboard/testimonial/create',name:'testimonialCreate',component:testimonialCreate,meta:{title:'قالوا عنا - إضافة'}},
    {path:'/dashboard/testimonial/:id',name:'testimonialShow',component:testimonialShow,meta:{title:'قالوا عنا - تعديل'}},

    // Test Route Of View
    {path:'/dashboard/test',component:TestView},
    //Contact
    {path:'/dashboard/contact',name:'contactIndex',component:contactIndex,meta:{title:'الرسائل الوارده'}},
];


const router = new VueRouter({
    routes,
    mode:'history',
    linkActiveClass:'active'
});

router.beforeEach((to, from, next) => {

    if(to.meta.title){
        document.title = to.meta.title;
    }else{
        document.title = "إنكو سيرفيس";
    }

    next();
});
export default router;