$(document).ready(function(){
    $('#resize-sidebar').on('click',function(){
        $('body').toggleClass('md-body');
    });

    $('body').css('padding-top',$('.navbar-default').height());

    $(window).on('resize',function(){
        $('body').css('padding-top',$('.navbar-default').height());

    });

    $('.main-menu ul > > li >a').on('click',function(){
        $(this).parent('li').addClass('active');
    });

});