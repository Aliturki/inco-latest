-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2017 at 11:21 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inco`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `small_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `image`, `small_img`, `author`, `content`, `small_content`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'من نكون', '2017-04-03_ec4gx9zgBc.jpeg', NULL, 'د/محمود برعي', '<p><time datetime="2017-04-03">منذ 7 ساعات، 3 أبريل,2017</time></p><p>ذاتَ صباح ربيعي، وقبل عيد النوروز بأيام، استيقظ العالم على مأساة حلبجة، ولم تستيقظ حلبجة نفسها، كادت الحرب الإيرانية &ndash; العراقية تلفظ أنفاسها الأخيرة بعد ثماني سنوات عجاف (1980 &ndash; 1988)، حين رأى القائد المفدَّى في العراق (صدام حسين) أن استرجاع حلبجة من أيدي الإيرانيين يقتضي تدميرها، دون اعتبار لآلاف النساء والأطفال الأكراد القابعين فيها، هكذا أطلق على الكيماوي (اسم أُطلق على مجرم حرب يُدعى علي حسن المجيد) بأوامر من القائد المفدَّى غازَي السارين والخردل على المدينة الوادعة، وفارق الحياة أمهات وفي أثدائهن أطفالهن الرضع، وانقطعت أنفاس أطفال على أبواب المدارس، وتوقف قلب فتيان يحلمون بالخفقة الأولى. عشرة آلاف شهيد كردي بجرة قلم، وعاش أسد العروبة وبعد قليل شهيد السُنَّة، وكأن حلبجة لم تكن شيئًا، ربما لأنهم ليسوا عربًا!</p><p>يقول معين بسيسو في أبيات ساقها لي صديق كردي عزيز:</p><blockquote><p align="center">كرديّا كان صلاح الدين *** انتصَر فأصبحَ عربيّا</p><p align="center">ماذا لو هُزِم صلاح الدين؟! *** لكان جاسوسًا كرديّا</p></blockquote>', 'ذاتَ صباح ربيعي، وقبل عيد النوروز بأيام، استيقظ العالم على مأساة حلبجة، ولم تستيقظ حلبجة نفسها، كادت الحرب الإيرانية', 1, '2017-04-03 16:33:26', '2017-04-09 10:18:26'),
(2, 'هذا انا', '2017-04-03_vkLPtl2fFr.jpeg', NULL, 'د/محمود برعي', '<p><a href="https://www.sasapost.com/" rel="nofollow"><br><img alt="" src="https://www.sasapost.com/wp-content/themes/sasapost/library/images/sasa-logo-1x.png" srcset="https://www.sasapost.com/wp-content/themes/sasapost/library/images/sasa-logo-1x.png 1x, <a href=" class="fr-fic fr-dii">https://www.sasapost.com/wp-content/themes/sasapost/library/images/sasa-logo-2x.png</a> 2x&quot;&gt;</p><h3><a href="https://www.sasapost.com/tag/%d8%a7%d9%84%d8%a3%d9%83%d8%b1%d8%a7%d8%af/">الأكراد</a>حلبجة.. تنتصر أخيرًا</h3><p>حجم الخط<a href="https://www.sasapost.com/opinion/halabja-finally-wins/#">س</a><a href="https://www.sasapost.com/opinion/halabja-finally-wins/#">س</a><a href="https://www.sasapost.com/opinion/halabja-finally-wins/#">س</a></p><nav><ul><li><a href="https://www.sasapost.com/category/policy/">سياسة</a></li><li><a href="https://www.sasapost.com/category/community/">مجتمع</a></li><li><a href="https://www.sasapost.com/category/translations/">ترجمات</a></li><li><a href="https://www.sasapost.com/category/art-and-entertainment/">فن وترفيه</a></li><li><a href="https://www.sasapost.com/category/science-and-technology/">علوم وتكنولوجيا</a></li><li><a href="https://www.sasapost.com/opinions/">مقالات الرأي</a></li><li><a href="https://www.sasapost.com/sasarank/">ساسة رانك</a></li><li><a href="https://www.sasapost.com/editor/">اكتب معنا</a></li></ul></nav><p>&nbsp;<a href="https://www.twitter.com/sasapostmedia/" target="_blank">&nbsp;</a></p><header><h1 itemprop="headline">حلبجة.. تنتصر أخيرًا</h1></header><article><section itemprop="articleBody"><a href="https://www.sasapost.com/author/ahmad-salah-2/"><img src="https://graph.facebook.com/10212781315467225/picture?type=large" alt="default-avatar" class="fr-fic fr-dii">أحمد صلاحهاشم</a><time datetime="2017-04-03">منذ 7 ساعات، 3 أبريل,2017</time><p>ذاتَ صباح ربيعي، وقبل عيد النوروز بأيام، استيقظ العالم على مأساة حلبجة، ولم تستيقظ حلبجة نفسها، كادت الحرب الإيرانية &ndash; العراقية تلفظ أنفاسها الأخيرة بعد ثماني سنوات عجاف (1980 &ndash; 1988)، حين رأى القائد المفدَّى في العراق (صدام حسين) أن استرجاع حلبجة من أيدي الإيرانيين يقتضي تدميرها، دون اعتبار لآلاف النساء والأطفال الأكراد القابعين فيها، هكذا أطلق على الكيماوي (اسم أُطلق على مجرم حرب يُدعى علي حسن المجيد) بأوامر من القائد المفدَّى غازَي السارين والخردل على المدينة الوادعة، وفارق الحياة أمهات وفي أثدائهن أطفالهن الرضع، وانقطعت أنفاس أطفال على أبواب المدارس، وتوقف قلب فتيان يحلمون بالخفقة الأولى. عشرة آلاف شهيد كردي بجرة قلم، وعاش أسد العروبة وبعد قليل شهيد السُنَّة، وكأن حلبجة لم تكن شيئًا، ربما لأنهم ليسوا عربًا!</p><p>يقول معين بسيسو في أبيات ساقها لي صديق كردي عزيز:</p><blockquote><p align="center">كرديّا كان صلاح الدين *** انتصَر فأصبحَ عربيّا</p><p align="center">ماذا لو هُزِم صلاح الدين؟! *** لكان جاسوسًا كرديّا</p></blockquote><p>في الحقيقة، لم تكن الأزمةَ الأولى التي تصادف رحلة الأكراد من أجل تحقيق استقلالهم ولفت نظر التاريخ؛ فمنذ غزو ساسان، وحتى الحرب السورية، لا يزال آل صلاح الدين مشتتين بين إيران والعراق وسوريا وتركيا. تعزلهم إيران، وتهمِّشهم سوريا، وتقتلهم تركيا. ويعاندهم العراق، وهم في الأردن ضيوف شرف، وفي أرمينيا تذكير بمأساة صفحاتها لم ولن تنطوي عما قريب!</p><p>فإن كان العراق وإيران قد اعترفا على مَضَض بمناطق كردية على أراضيهما: في الأول إقليم دون حدود، وفي الثانية إقليم شبه مستقل يتنازع حقوق بيع بتروله مع حكومة عراقية من صنع إيران، فإن قضيتهم لا يحملها عنهم غيرهم ولا يدرك الإعلام عدالة مطالبهم، أو يدرك ويتجاهل، فهم خمسة وثلاثون مليون مناضل فحسب!</p><p>أتعرفون أن تركيا كانت على وشك الدخول في حرب مع سوريا قبل سنوات؟! وأن إيران كانت تقاتل العراق في حرب السنين العجاف؟! هؤلاء الخصوم الأربعة لم يجتمعوا على طريقة سلق بيضة، لكنهم اتفقوا على وجوب قتال الأكراد، ولمَ لا، والكرد يحترفون قول: لا!</p><p>قال الشاعر فاروق جويدة من قبل:</p><blockquote><p>قطعوا لساني</p><p>قـطـعوه يومًا عنـدما سمعوه</p><p>يصرخ في براءته القـديمة</p><p>عنـد أعتاب الكبار:</p><p>إني أحبّ ولا أحب</p><p>إني أريد ولا أريد</p><p>قالوا جميعًا: كيف (لا) دخلت لقاموس الصغار؟!</p></blockquote><p>شعب مجاهد بطبعه، كأن النضال خُلِق له، تراهم بسروالهم ولباسهم الفضفاض، فتوقن أنهم جنود في استراحة محارب، تتعجب من إرادتهم التي هي أقوى من فولاذ الحديد، أمضى من سيوف الجيران.</p><p>هل جربتَ أن تزور أربيل أو السليمانية؟ هل سمعت عن كركوك أو دهوك؟ مدنهم صورة منهم، كما يقول الشاعر:</p><blockquote><p align="center">لا يُشبِه الشعراءُ يُشبِه شعرَه *** إن البناءَ الفذّ يُشبِه مَن بَنَى</p></blockquote><p>لهذا كانت بناياتهم شامخات على النمط الأوروبي، تحرُّر دون تناسي الدين، وانفتاح على العالم دون هتك الهوية، وجيوش يدافع فيها النساء والرجال عن حقوق الوطن، تتعاضد فيها قيم المساواة في الميدان حقيقة ناصعة، وليست شعارات براقة، تدافع عن المرأة العربية وجه النهار، وتجلدها بالسياط قلب الليل.</p><p>خمسة وثلاثون مليونًا يبحثون عن بقعة أرض من العراق إلى أرمينيا، ويتنازل العرب عن أعظم مقدساتهم لثمانية ملايين صهيوني كانوا حتى الأمس في الشتات!</p><p>حكمة الأكراد عَرَّفَتْهم أن جيرانهم العرب لا يقدمون شيئًا فلم ينتظروا شيئًا من العرب. حملوا سلاحهم، وعند حدود مناطقهم وقفوا يمنعون الغزاة، &laquo;داعش&raquo; كانوا، أو إيرانيين، أو جيوشًا المفترض أنها نظامية تقتل شعوبها برصاص الغدر المصبوب، أو زعماء يتلطّون بالدين ويحاولون تصفير مشكلاتهم الداخلية عبر انتهاك آدمية شعوب تسعى لإرساء مفاهيم التمدُّن والحضارة!</p><p>أعيادهم الوطنية صورة منهم؛ ثقافة اللون والسلام والحب والتضامن، تعرفهم من حماستهم عندما يتحدثون عن بلادهم السليبة، وصراعهم من أجل الوجود، وهدفهم الواضح الذي يتشبثون به ويسعون إليه على أرواحهم دون تطرف أو كثير صراخ: الوطن، الوطن فحسب. وطن يأمنون فيه على أرواحهم وقلوبهم، وينامون، دون أن يستيقظوا على حلبجة جديدة!</p><p>هذا المقال يعبر عن رأي كاتبه ولا يعبر بالضرورة عن ساسة بوست</p></section><footer><h3>تعليقات الفيسبوك</h3><h3>مقالات ذات صلة</h3><article><h3><a href="https://www.sasapost.com/albab-raqa-mosul/">الباب والرقة والموصل.. الحسم ضد &laquo;تنظيم الدولة&raquo; لايزال مبكرًا</a></h3><ul><li>منذ شهر واحد</li></ul></article><article><h3><a href="https://www.sasapost.com/black-democratic-turkey/">&laquo;التعذيب في تركيا&raquo;.. حين وصلت &laquo;ديمقراطية السود&raquo; لأفق مسدود</a></h3><ul><li>منذ 4 شهور</li><li>3,162</li></ul></article><article><h3><a href="https://www.sasapost.com/kurdish-turkish-conflict/">بسبب &laquo;معركة الموصل&raquo;.. العلاقات التركية الكردية إلى نفق مسدود</a></h3><ul><li>منذ 5 شهور</li></ul></article></footer></article><h2>اختيار الموقع</h2><article><h3><a href="https://www.sasapost.com/turkeys_regimes_story/">قصة السلطة في تركيا من أتاتورك حتى أردوغان</a></h3></article><article><h3><a href="https://www.sasapost.com/was-islamic-invasions-colonial-wars/">هل كانت الغزوات الإسلامية حروبًا استعمارية؟</a></h3></article><article><h3><a href="https://www.sasapost.com/translation/the-indian-ceos-of-worlds-best/">مترجم: لماذا يدير الهنود العديد من أفضل شركات العالم؟</a></h3></article><article><h3><a href="https://www.sasapost.com/your-guide-to_professional-translation/">دليلك لترجمة أكثر احترافية</a></h3></article><h2>آراء</h2><article><a href="https://www.sasapost.com/author/yakoub-madi/"><img src="https://www.sasapost.com/wp-content/uploads/IMGd_0472_webcamera360_20170105174053-100x100.jpg" srcset="https://www.sasapost.com/wp-content/uploads/IMGd_0472_webcamera360_20170105174053-100x100.jpg 1x,https://www.sasapost.com/wp-content/uploads/IMGd_0472_webcamera360_20170105174053-200x200.jpg 2x" alt="" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/universitydie/">الجامعات العربية بين الحياة والموت</a></h3><a href="https://www.sasapost.com/author/yakoub-madi/">يعقوب ماضي</a></article><article><a href="https://www.sasapost.com/author/ahmed-tareg/"><img src="https://graph.facebook.com/1269212133146178/picture?type=large" alt="default-avatar" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/prime-minister-in-sudan/">اختيار &nbsp;رئيس الوزراء في السودان!</a></h3><a href="https://www.sasapost.com/author/ahmed-tareg/">احمد طارق حسن</a></article><article><a href="https://www.sasapost.com/author/islam-mansour/"><img src="https://graph.facebook.com/437721829914076/picture?type=large" alt="default-avatar" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/do-not-bow-down-to-your-idol/">لا تَسْجُد لصَنَمِكَ</a></h3><a href="https://www.sasapost.com/author/islam-mansour/">Islam Mansour</a></article><article><a href="https://www.sasapost.com/author/ahmad-salah-2/"><img src="https://graph.facebook.com/10212781315467225/picture?type=large" alt="default-avatar" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/halabja-finally-wins/">حلبجة.. تنتصر أخيرًا</a></h3><a href="https://www.sasapost.com/author/ahmad-salah-2/">أحمد صلاح</a></article><article><a href="https://www.sasapost.com/author/adham-kahlawi/"><img src="https://www.sasapost.com/wp-content/uploads/photo-5823-100x100.jpg" srcset="https://www.sasapost.com/wp-content/uploads/photo-5823-100x100.jpg 1x,https://www.sasapost.com/wp-content/uploads/photo-5823-200x200.jpg 2x" alt="" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/mechanism-of-thinking-of-islamic-organizations/">آلية تفكير التنظيمات الإسلامية والغرب</a></h3><a href="https://www.sasapost.com/author/adham-kahlawi/">أدهم كحلاوي</a></article><article><a href="https://www.sasapost.com/author/sameh-khatab/"><img src="https://www.sasapost.com/wp-content/uploads/photo-6492-100x100.jpg" srcset="https://www.sasapost.com/wp-content/uploads/photo-6492-100x100.jpg 1x,https://www.sasapost.com/wp-content/uploads/photo-6492-200x200.jpg 2x" alt="" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/war-is-a-debate-why-do-arabs-lose/">الحرب سجال والأيام دول.. لماذا يخسر العرب؟</a></h3><a href="https://www.sasapost.com/author/sameh-khatab/">سامح خطاب</a></article><article><a href="https://www.sasapost.com/author/1254/"><img src="https://www.sasapost.com/wp-content/uploads/photo-6491-100x100.jpg" srcset="https://www.sasapost.com/wp-content/uploads/photo-6491-100x100.jpg 1x,https://www.sasapost.com/wp-content/uploads/photo-6491-200x200.jpg 2x" alt="" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/independence-of-the-judiciary/">استقلال القضاء وممارساته&nbsp;</a></h3><a href="https://www.sasapost.com/author/1254/">د- محمد أحمد</a></article><article><a href="https://www.sasapost.com/author/abdennour-afkir/"><img src="https://graph.facebook.com/624293851102193/picture?type=large" alt="default-avatar" class="fr-fic fr-dii"></a><h3><a href="https://www.sasapost.com/opinion/about-palestine-2/">فلسطين.. محاولة يائسة للاستقلال بمفهوم</a></h3><a href="https://www.sasapost.com/author/abdennour-afkir/">Abdennour Afkir</a></article><h2>أحدث الموضوعات</h2><article><h3><a href="https://www.sasapost.com/translation/middle-kingdom-meets-middle-east/">&laquo;أمريكان إنترست&raquo;: كيف تسعى الصين إلى احتلال الدور الأمريكي في الشرق الأوسط؟</a></h3></article><article><h3><a href="https://www.sasapost.com/translation/the-science-of-fear/">&laquo;سي إن إن&raquo;: الخوف.. كيف يعالجه المخ وهل يصبح إدمانًا لدى البعض؟</a></h3></article><article><h3><a href="https://www.sasapost.com/right-wings-more-attractive/">المحافظون أكثر جمالًا وجاذبية من السياسيين الليبراليين.. والناخب هو السبب</a></h3></article><article><h3><a href="https://www.sasapost.com/economy-egypt-poor/">كيف قضى صندوق النقد الدولي على نصيب الفقراء في موازنة مصر الجديدة؟</a></h3></article><h2>الأكثر قراءة</h2><article><h3><a href="https://www.sasapost.com/how-do-you-get-a-quarter-of-a-million-views-in-less-than-a-month/">بسنت نور الدين.. أو كيف تحصل على ربع مليون مشاهدة في أقل من شهر؟</a></h3></article><article><h3><a href="https://www.sasapost.com/translation/trump-first-100-days/">&laquo;واشنطن بوست&raquo;: هذا ما حدث لـ&laquo;ترامب&raquo; في اليوم الـ70 له في البيت الأبيض</a></h3></article><article><h3><a href="https://www.sasapost.com/congress-and-internet-privacy/">في أمريكا: تاريخ تصفحك للإنترنت أصبح للبيع لمن يدفع أكثر.. وهكذا تحمي نفسك</a></h3></article><article><h3><a href="https://www.sasapost.com/translation/muslim-men-hijab-forcing-women-islam-teaching-mohammed-quran-modesty/">&laquo;الإندبندنت&raquo;: على الرجال المسلمين غض البصر قبل فرض الحجاب على النساء</a></h3></article><h2>المقالات الصاعدة</h2><article><h3><a href="https://www.sasapost.com/translation/muslim-men-hijab-forcing-women-islam-teaching-mohammed-quran-modesty/">&laquo;الإندبندنت&raquo;: على الرجال المسلمين غض البصر قبل فرض الحجاب على النساء</a></h3></article><article><h3><a href="https://www.sasapost.com/best-free-courses-available-in-arabic-during-april/">أفضل &laquo;الكورسات&raquo; المجانية المتاحة باللغة العربية خلال شهر أبريل</a></h3></article><article><h3><a href="https://www.sasapost.com/chronotype-test/">اختبار علمي: هل أنت شخص صباحي أم مسائي؟</a></h3></article><article><h3><a href="https://www.sasapost.com/how-do-you-get-a-quarter-of-a-million-views-in-less-than-a-month/">بسنت نور الدين.. أو كيف تحصل على ربع مليون مشاهدة في أقل من شهر؟</a></h3></article><footer><a href="https://www.sasapost.com/" rel="nofollow"><img alt="" src="https://www.sasapost.com/wp-content/themes/sasapost/library/images/sasa-logo-white-1x.png" srcset="https://www.sasapost.com/wp-content/themes/sasapost/library/images/sasa-logo-white-1x.png 1x, <a href=" class="fr-fic fr-dii">https://www.sasapost.com/wp-content/themes/sasapost/library/images/sasa-logo-white-2x.png</a> 2x&quot;&gt;<p>أفكار حرة تقرأ العالم والأحداث عبر عيون الإنسان العربي</p><h3><a href="https://www.sasapost.com/category/translations/" target="_blank">ترجمات</a></h3><h3><a href="https://www.sasapost.com/category/policy/" target="_blank">سياسة</a></h3><ul><li><a href="https://www.sasapost.com/tag/arab-world/">عربي</a></li><li><a href="https://www.sasapost.com/category/economy/">اقتصاد</a></li><li><a href="https://www.sasapost.com/tag/international/">دولي</a></li><li><a href="https://www.sasapost.com/tag/freedoms/">حريات</a></li><li><a href="https://www.sasapost.com/tag/military/">عسكري</a></li></ul><h3><a href="https://www.sasapost.com/opinions/" target="_blank">آراء</a></h3><h3><a href="https://www.sasapost.com/category/community/" target="_blank">مجتمع</a></h3><ul><li><a href="https://www.sasapost.com/tag/%d8%aa%d8%b1%d8%a8%d9%8a%d8%a9/">تربية</a></li><li><a href="https://www.sasapost.com/tag/woman/">المرأة</a></li><li><a href="https://www.sasapost.com/tag/media/">إعلام</a></li><li><a href="https://www.sasapost.com/tag/%d8%aa%d8%b9%d9%84%d9%8a%d9%85/">تعليم</a></li><li><a href="https://www.sasapost.com/tag/books/">كتب</a></li><li><a href="https://www.sasapost.com/tag/social-media/">سوشيال ميديا</a></li></ul><h3><a href="https://www.sasapost.com/category/science-and-technology/" target="_blank">علوم وتكنولوجيا</a></h3><h3><a href="https://www.sasapost.com/category/infographic/" target="_blank">إنفوجرافيك</a></h3><h3><a href="https://www.sasapost.com/category/art-and-entertainment/" target="_blank">فن وترفيه</a></h3><ul><li><a href="https://www.sasapost.com/tag/cinema/">سينما</a></li><li><a href="https://www.sasapost.com/tag/travel/">سفر</a></li><li><a href="https://www.sasapost.com/tag/sport/">رياضة</a></li></ul><h3><a href="https://www.sasapost.com/editor/" target="_blank">اكتب معنا</a></h3><h3><a href="https://www.sasapost.com/contact-us/" target="_blank">اتصل بنا</a></h3><h3><a href="https://www.sasapost.com/about/" target="_blank">من نحن</a></h3></footer>', '(صدام حسين) أن استرجاع حلبجة من أيدي الإيرانيين يقتضي تدميرها', 1, '2017-04-03 16:37:19', '2017-04-09 10:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `contactuses`
--

CREATE TABLE `contactuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_read` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactuses`
--

INSERT INTO `contactuses` (`id`, `name`, `email`, `phone`, `subject`, `ip`, `_read`, `message`, `created_at`, `updated_at`) VALUES
(10, 'على تركي', 'aliturki2020@gmail.com', '01210132015', 'السلام عليكم ورحمة الله وبركاته', '127.0.0.1', 1, 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.', '2017-04-14 11:18:14', '2017-04-14 11:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_11_151213_create_site_settings_table', 1),
(4, '2017_03_22_152709_create_sliders_table', 2),
(5, '2017_03_25_085126_create_static_pages_table', 2),
(6, '2017_03_25_085440_create_articles_table', 2),
(7, '2017_03_25_115031_create_services_table', 2),
(8, '2017_04_03_190454_create_testimonials_table', 3),
(9, '2017_04_04_152733_create_subscribes_table', 4),
(10, '2016_09_12_99999_create_visitlogs_table', 5),
(11, '2017_04_09_141857_create_test', 6),
(12, '2017_04_09_170736_create_contactuses_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `description`, `content`, `is_active`, `created_at`, `updated_at`) VALUES
(6, 'عنوان الخدمه', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', '\\\\\\هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\\\\\\', 1, '2017-04-06 10:09:57', '2017-04-06 10:43:31'),
(7, 'الخدمات', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', '\\هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\\', 1, '2017-04-06 10:10:11', '2017-04-06 10:10:11'),
(8, 'service', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', '\\هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\\', 1, '2017-04-06 10:39:48', '2017-04-06 10:39:48'),
(9, 'servic e2', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', '\\\\هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\\\\', 1, '2017-04-06 10:40:54', '2017-04-06 10:43:51'),
(10, 'الدرسات البيئيه', 'ويعترف كول بأن التحليل يستند إلى القيمة الغذائية لمجموعة صغيرة من البشر الذكور الحديثة، في حين كان الإنسان البدائي أكثر قوة. ويقول «إن البحث يشير إلى أن آكلي لحوم البشر قد يكونون مدفوعين بمجموعة معقدة من الدوافع». وأضاف «لا تقول الدراسة إننا لم نأكل بعضنا البعض لأسباب غذائية في الماضي، لكن ما تقترحه هو أن هذا التفسير ليس هو الوحيد. ربما يكون هناك المزيد من السياقات الاجتماعي هنا، ليس الطقوس الدينية على وجه التحديد، لكن الاجتماعية».', '\\التغذية ليست التفسير الوحيد\\\\ويعترف كول بأن التحليل يستند إلى القيمة الغذائية لمجموعة صغيرة من البشر الذكور الحديثة، في حين كان الإنسان البدائي أكثر قوة. ويقول &\\l\\a\\q\\u\\o;إن البحث يشير إلى أن آكلي لحوم البشر قد يكونون مدفوعين بمجموعة معقدة من الدوافع&\\r\\a\\q\\u\\o;. وأضاف &\\l\\a\\q\\u\\o;لا تقول الدراسة إننا لم نأكل بعضنا البعض لأسباب غذائية في الماضي، لكن ما تقترحه هو أن هذا التفسير ليس هو الوحيد. ربما يكون هناك المزيد من السياقات الاجتماعي هنا، ليس الطقوس الدينية على وجه التحديد، لكن الاجتماعية&\\r\\a\\q\\u\\o;.\\\\ومن بين الاحتمالات التي اقترحها كول كانت المتعلقة بأن آكلي لحوم البشر قد يكونون انتهازيين، يستغلون الفرصة ويتناولون البشر عندما يموتون نتيجة لأسباب طبيعية، أو أن الأمر قد يكون \\مرتبطًا بعملية الدفاع الإقليمي\\، وحماية المحيط الخاص بهم.\\\\ورحب بول بيتيت، أستاذ علم الآثار في العصر الحجري القديم في جامعة دورهام، بهذا البحث، مشيرًا إلى أن الرئيسيات (شعبة الرئيسيات التي تضم البشر) بما في ذلك البونوبوس والشمبانزي تظهر أيضًا أدلة على أكل لحوم البشر. وقال &\\l\\a\\q\\u\\o;إن مثل هذه السلوكيات تشكل بوضوح شيئًا مثل الطقوس السلوكية &\\n\\d\\a\\s\\h; وهو عمل فاقد للوعي ينبع من أنشطة مشتركة مركزية لسلوك المجموعات، مثل تناول اللحوم&\\r\\a\\q\\u\\o;.\\\\وأضاف &\\l\\a\\q\\u\\o;في مكان ما على طول خط التطور البشري تحول هذا السلوك من الطقوس السلوكية إلى سلوك طقوسي، وكما يظهر كول بشكل جيد جدًا، فإن الأدلة تكشف بوضوح أن تناول اللحوم البشرية لم يكن حصريًا بهدف البقاء على قيد الحياة&\\r\\a\\q\\u\\o;.\\\\وتتفق سيلفيا بيلو من متحف التاريخ الطبيعي مع كول بأن أكل لحوم البشر في العصر الحجري القديم غالبًا ما كان يمارس كخيار بدلًا عن البقاء على قيد الحياة، لكنها أضافت أن الوصول بشكل دقيق إلى الدوافع وراء هذا الخيار هو عمل صعب للغاية. وأضافت &\\l\\a\\q\\u\\o;هل كان أكل لحوم البشر بمثابة طقوس جنائزية، أم أنها مجرد وسيلة لتناول ما كان متاحًا؟ أنا لست متأكدة من أن الأدلة يمكن أن تساعد حقا لاختيار تفسير هذا الخيار أو ذلك&\\r\\a\\q\\u\\o;.\\\\وأوضحت في ختام حديثها أنهم كعلماء بحاجة فقط للحفاظ على البحث عن أساليب جديدة للتحليلات والوصول إلى مواقع جديدة لفهم أفضل لهذا السلوك.\\\\البدائيون كانوا أذكياء\\\\\\ومما يؤكد فرضية عدم اعتماد الإنسان البدائي على لحوم البشر كغذاء بشكل رئيسي، هو ما اكتشفه العلماء حديثًا من استخدامات وسلوكيات أكثر ذكاءً للإنسان البدائي مما كنا نعتقد سابقًا. فبعض البشر البدائيين اتبعوا بالفعل نظامًا غذائيًّا نباتيًا، كما يبدو أنهم استخدموا الأشكال الطبيعية من البنسلين والأسبرين لعلاج الالتهابات والألم.\\\\\\تمكن باحثون\\ من العثور على مادة البلاك (\\p\\l\\a\\q\\u\\e) مثبتًا على الأسنان المتحجرة للبشر البدائيين الذين عاشوا في القارة الأوروبية، وتحديدًا منطقة أستورياس في إسبانيا، بالإضافة إلى إحدى مناطق بلجيكا، والتي يعود تاريخها إلى قرابة 50 ألف سنة. وفي عرض مذهل يبين مدى الإدهاش الذي يمكن أن يكون عليه العلم، كان فريق من الباحثين من إسبانيا وأستراليا والمملكة المتحدة قادر على استخراج الحمض النووي من هذه المادة التي غالبًا ما تضم بقايا الطعام، وهو ما سمح لهم بمعرفة ما كان يأكله هؤلاء الأشخاص.\\', 1, '2017-04-09 08:32:38', '2017-04-09 08:32:38'),
(11, 'التغذية ليست التفسير الوحيد', 'ومن بين الاحتمالات التي اقترحها كول كانت المتعلقة بأن آكلي لحوم البشر قد يكونون انتهازيين، يستغلون الفرصة ويتناولون البشر عندما يموتون نتيجة لأسباب طبيعية، أو أن الأمر قد يكون مرتبطًا بعملية الدفاع الإقليمي، وحماية المحيط الخاص بهم.', 'التغذية ليست التفسير الوحيد<p>ويعترف كول بأن التحليل يستند إلى القيمة الغذائية لمجموعة صغيرة من البشر الذكور الحديثة، في حين كان الإنسان البدائي أكثر قوة. ويقول &laquo;إن البحث يشير إلى أن آكلي لحوم البشر قد يكونون مدفوعين بمجموعة معقدة من الدوافع&raquo;. وأضاف &laquo;لا تقول الدراسة إننا لم نأكل بعضنا البعض لأسباب غذائية في الماضي، لكن ما تقترحه هو أن هذا التفسير ليس هو الوحيد. ربما يكون هناك المزيد من السياقات الاجتماعي هنا، ليس الطقوس الدينية على وجه التحديد، لكن الاجتماعية&raquo;.</p><p>ومن بين الاحتمالات التي اقترحها كول كانت المتعلقة بأن آكلي لحوم البشر قد يكونون انتهازيين، يستغلون الفرصة ويتناولون البشر عندما يموتون نتيجة لأسباب طبيعية، أو أن الأمر قد يكون مرتبطًا بعملية الدفاع الإقليمي، وحماية المحيط الخاص بهم.</p><p>ورحب بول بيتيت، أستاذ علم الآثار في العصر الحجري القديم في جامعة دورهام، بهذا البحث، مشيرًا إلى أن الرئيسيات (شعبة الرئيسيات التي تضم البشر) بما في ذلك البونوبوس والشمبانزي تظهر أيضًا أدلة على أكل لحوم البشر. وقال &laquo;إن مثل هذه السلوكيات تشكل بوضوح شيئًا مثل الطقوس السلوكية &ndash; وهو عمل فاقد للوعي ينبع من أنشطة مشتركة مركزية لسلوك المجموعات، مثل تناول اللحوم&raquo;.</p><p>وأضاف &laquo;في مكان ما على طول خط التطور البشري تحول هذا السلوك من الطقوس السلوكية إلى سلوك طقوسي، وكما يظهر كول بشكل جيد جدًا، فإن الأدلة تكشف بوضوح أن تناول اللحوم البشرية لم يكن حصريًا بهدف البقاء على قيد الحياة&raquo;.</p><p>وتتفق سيلفيا بيلو من متحف التاريخ الطبيعي مع كول بأن أكل لحوم البشر في العصر الحجري القديم غالبًا ما كان يمارس كخيار بدلًا عن البقاء على قيد الحياة، لكنها أضافت أن الوصول بشكل دقيق إلى الدوافع وراء هذا الخيار هو عمل صعب للغاية. وأضافت &laquo;هل كان أكل لحوم البشر بمثابة طقوس جنائزية، أم أنها مجرد وسيلة لتناول ما كان متاحًا؟ أنا لست متأكدة من أن الأدلة يمكن أن تساعد حقا لاختيار تفسير هذا الخيار أو ذلك&raquo;.</p><p>وأوضحت في ختام حديثها أنهم كعلماء بحاجة فقط للحفاظ على البحث عن أساليب جديدة للتحليلات والوصول إلى مواقع جديدة لفهم أفضل لهذا السلوك.</p>البدائيون كانوا أذكياء<p>ومما يؤكد فرضية عدم اعتماد الإنسان البدائي على لحوم البشر كغذاء بشكل رئيسي، هو ما اكتشفه العلماء حديثًا من استخدامات وسلوكيات أكثر ذكاءً للإنسان البدائي مما كنا نعتقد سابقًا. فبعض البشر البدائيين اتبعوا بالفعل نظامًا غذائيًّا نباتيًا، كما يبدو أنهم استخدموا الأشكال الطبيعية من البنسلين والأسبرين لعلاج الالتهابات والألم.</p><p>تمكن باحثون من العثور على مادة البلاك (plaque) مثبتًا على الأسنان المتحجرة للبشر البدائيين الذين عاشوا في القارة الأوروبية، وتحديدًا منطقة أستورياس في إسبانيا، بالإضافة إلى إحدى مناطق بلجيكا، والتي يعود تاريخها إلى قرابة 500 ألف سنة. وفي عرض مذهل يبين مدى الإدهاش الذي يمكن أن يكون عليه العلم، كان فريق من الباحثين من إسبانيا وأستراليا والمملكة المتحدة قادر على استخراج الحمض النووي من هذه المادة التي غالبًا ما تضم بقايا الطعام، وهو ما سمح لهم بمعرفة ما كان يأكله هؤلاء الأشخاص.</p>', 1, '2017-04-09 08:38:05', '2017-04-09 08:38:05'),
(12, 'الخدمات المركبه', 'ويعترف كول بأن التحليل يستند إلى القيمة الغذائية لمجموعة صغيرة من البشر الذكور الحديثة، في حين كان الإنسان البدائي أكثر قوة. ويقول «إن البحث يشير إلى أن آكلي لحوم البشر قد يكونون مدفوعين بمجموعة معقدة من الدوافع». وأضاف «لا تقول الدراسة إننا لم نأكل بعضنا البعض لأسباب', '<h2>التغذية ليست التفسير الوحيد</h2><p>ويعترف كول بأن التحليل يستند إلى القيمة الغذائية لمجموعة صغيرة من البشر الذكور الحديثة، في حين كان الإنسان البدائي أكثر قوة. ويقول &laquo;إن البحث يشير إلى أن آكلي لحوم البشر قد يكونون مدفوعين بمجموعة معقدة من الدوافع&raquo;. وأضاف &laquo;لا تقول الدراسة إننا لم نأكل بعضنا البعض لأسباب غذائية في الماضي، لكن ما تقترحه هو أن هذا التفسير ليس هو الوحيد. ربما يكون هناك المزيد من السياقات الاجتماعي هنا، ليس الطقوس الدينية على وجه التحديد، لكن الاجتماعية&raquo;.</p><p>ومن بين الاحتمالات التي اقترحها كول كانت المتعلقة بأن آكلي لحوم البشر قد يكونون انتهازيين، يستغلون الفرصة ويتناولون البشر عندما يموتون نتيجة لأسباب طبيعية، أو أن الأمر قد يكون مرتبطًا بعملية الدفاع الإقليمي، وحماية المحيط الخاص بهم.</p><p>ورحب بول بيتيت، أستاذ علم الآثار في العصر الحجري القديم في جامعة دورهام، بهذا البحث، مشيرًا إلى أن الرئيسيات (شعبة الرئيسيات التي تضم البشر) بما في ذلك البونوبوس والشمبانزي تظهر أيضًا أدلة على أكل لحوم البشر. وقال &laquo;إن مثل هذه السلوكيات تشكل بوضوح شيئًا مثل الطقوس السلوكية &ndash; وهو عمل فاقد للوعي ينبع من أنشطة مشتركة مركزية لسلوك المجموعات، مثل تناول اللحوم&raquo;.</p><p>وأضاف &laquo;في مكان ما على طول خط التطور البشري تحول هذا السلوك من الطقوس السلوكية إلى سلوك طقوسي، وكما يظهر كول بشكل جيد جدًا، فإن الأدلة تكشف بوضوح أن تناول اللحوم البشرية لم يكن حصريًا بهدف البقاء على قيد الحياة&raquo;.</p><p>وتتفق سيلفيا بيلو من متحف التاريخ الطبيعي مع كول بأن أكل لحوم البشر في العصر الحجري القديم غالبًا ما كان يمارس كخيار بدلًا عن البقاء على قيد الحياة، لكنها أضافت أن الوصول بشكل دقيق إلى الدوافع وراء هذا الخيار هو عمل صعب للغاية. وأضافت &laquo;هل كان أكل لحوم البشر بمثابة طقوس جنائزية، أم أنها مجرد وسيلة لتناول ما كان متاحًا؟ أنا لست متأكدة من أن الأدلة يمكن أن تساعد حقا لاختيار تفسير هذا الخيار أو ذلك&raquo;.</p><p>وأوضحت في ختام حديثها أنهم كعلماء بحاجة فقط للحفاظ على البحث عن أساليب جديدة للتحليلات والوصول إلى مواقع جديدة لفهم أفضل لهذا السلوك.</p><h2>البدائيون كانوا أذكياء</h2><p>ومما يؤكد فرضية عدم اعتماد الإنسان البدائي على لحوم البشر كغذاء بشكل رئيسي، هو ما اكتشفه العلماء حديثًا من استخدامات وسلوكيات أكثر ذكاءً للإنسان البدائي مما كنا نعتقد سابقًا. فبعض البشر البدائيين اتبعوا بالفعل نظامًا غذائيًّا نباتيًا، كما يبدو أنهم استخدموا الأشكال الطبيعية من البنسلين والأسبرين لعلاج الالتهابات والألم.</p><p>تمكن باحثون من العثور على مادة البلاك (plaque) مثبتًا على الأسنان المتحجرة للبشر البدائيين الذين عاشوا في القارة الأوروبية، وتحديدًا منطقة أستورياس في إسبانيا، بالإضافة إلى إحدى مناطق بلجيكا، والتي يعود تاريخها إلى قرابة 500 ألف سنة. وفي عرض مذهل يبين مدى الإدهاش الذي يمكن أن يكون عليه العلم، كان فريق من الباحثين من إسبانيا وأستراليا والمملكة المتحدة قادر على استخراج الحمض النووي من هذه المادة التي غالبًا ما تضم بقايا الطعام، وهو ما سمح لهم بمعرفة ما كان يأكله هؤلاء الأشخاص.</p>', 1, '2017-04-09 08:40:11', '2017-04-09 08:40:11'),
(13, 'على المدى المتوسط', 'وفي نفس الوقت – يضيف مات – تزداد أهمية ملحقات مثل مساعد أمازون Amazon Echo أو سماعة أبل AirPods. وكلما ازدادت فعالية أنظمة الذكاء الاصطناعي مثل Siri الخاص بأبل، لن يزداد فقط الحديث إلى الكمبيوتر، وإنما سيكون هناك رد من جانب الأخيرة.', '<h2>على المدى المتوسط</h2><p>يعتقد مات أن كل هذه التكنولوجيات المتنوعة والتي ما تزال في مراحلها الأولى ستتحول لاحقًا إلى شيء مألوف ولكن غريب.</p><p>إن كلًا من مايكروسوفت وفيسبوك وجوجل وماجيك ليب (المدعومة من جوجل) تعمل على ابتكار أجهزة الواقع المعزز الخاصة بها، حيث ستجلب تلك الأجهزة صورًا ثلاثية الأبعاد مبهرة مباشرة إليك. ويقال إن أبل تعتزم المنافسة في هذا المضمار أيضًا.</p><p>كان أليكس كبمان &ndash; المسؤول عن مشروع Hololens في شركة مايكروسوفت &ndash; قد أكد لموقع بيزنس إنسايدر أن تكنولوجيا الواقع المعزز قد تحل محل الهواتف الذكية والتلفزيونات وأي جهاز له شاشة. فلا فائدة لأجهزة منفصلة للرد على مكالماتك والدردشة ومشاهدة التلفاز وممارسة الألعاب إذا كان من الممكن أن تمارس كل ذلك عبر عينيك.</p><figure contenteditable="false"><img src="https://www.sasapost.com/wp-content/uploads/4334237247_08af133b4b_b-1.jpg" data-lazy-src="https://www.sasapost.com/wp-content/uploads/4334237247_08af133b4b_b-1.jpg" data-attachment="127256" class="fr-fic fr-dii"></figure><p>وفي نفس الوقت &ndash; يضيف مات &ndash; تزداد أهمية ملحقات مثل مساعد أمازون Amazon Echo أو سماعة أبل AirPods. وكلما ازدادت فعالية أنظمة الذكاء الاصطناعي مثل Siri الخاص بأبل، لن يزداد فقط الحديث إلى الكمبيوتر، وإنما سيكون هناك رد من جانب الأخيرة.</p>', 1, '2017-04-09 08:45:35', '2017-04-09 08:45:35');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_name`, `email`, `status`, `keywords`, `description`, `facebook`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'المركز الدولى للإسشتارات والخدمات العامه', 'info@inco.com', 1, 'استشارات بيئيه,تدورات تدريبيه,هندسه', 'التطبيق.\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى...', 'https://www.facebook.com', 'الهرم,امام مسرح الزعيم,الجيزه', 98653566, NULL, '2017-04-13 12:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `url`, `image`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(17, 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،', NULL, '2017-04-02_rKksiz3nr7.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،', 1, '2017-04-02 08:07:31', '2017-04-02 18:42:53'),
(21, 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى', NULL, '2017-04-02_Qfhgb3xeSt.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى', 1, '2017-04-02 10:46:59', '2017-04-02 18:42:28'),
(23, 'هذا النص هو مثال لنص يمكن أن يس', NULL, '2017-04-06_CElftQBo95.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربىهذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى', 1, '2017-04-02 16:42:10', '2017-04-06 09:44:26'),
(25, 'لمن نكون', NULL, '2017-04-06_ajNmhNxgzZ.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:19:31', '2017-04-06 10:19:31'),
(26, 'لو لم نكن', NULL, '2017-04-06_WHWFp2Tub7.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:20:30', '2017-04-06 10:20:30'),
(27, 'سنكون هناك', NULL, '2017-04-06_02lKmKd9Mf.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:21:19', '2017-04-06 10:21:19'),
(28, 'إلى مِن و مِن َمن', NULL, '2017-04-06_kAFAOi6mL0.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:22:34', '2017-04-06 10:22:34'),
(29, 'ليس هناك من هنال', NULL, '2017-04-06_OYCSfjsfBA.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:23:16', '2017-04-06 10:23:16'),
(30, 'تجريبي', NULL, '2017-04-06_OPfGEn0rFq.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:29:08', '2017-04-06 10:29:08'),
(31, 'تجريب 2', NULL, '2017-04-06_gEK40Q2Eeq.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:29:21', '2017-04-06 10:29:21'),
(32, 'yesnos', NULL, '2017-04-06_0IJ3sfyLRy.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:31:49', '2017-04-06 10:31:49'),
(33, 'yes we', NULL, '2017-04-06_D0z4ISOxB3.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 0, '2017-04-06 10:32:06', '2017-04-06 10:32:06'),
(34, 'aaaaaaaaa', NULL, '2017-04-06_vfVGvZAYXK.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:33:59', '2017-04-06 10:33:59'),
(35, 'swdsdfdsvf', NULL, '2017-04-06_0yt3aVeBxl.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:34:30', '2017-04-06 10:34:30'),
(36, 'awfefesfww', NULL, '2017-04-06_M7lJrDgDhN.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:34:38', '2017-04-06 10:34:38'),
(37, 'awfefsdcd', NULL, '2017-04-06_LOZ10pOdx4.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 0, '2017-04-06 10:35:10', '2017-04-06 10:35:10'),
(38, 'axscsacascsac', NULL, '2017-04-06_RKHt5KuCHn.jpeg', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-06 10:35:24', '2017-04-06 10:35:24'),
(39, 'خدمات تحت رعاية شركتنا', 'info@service.com', '2017-04-10_hJajqM4EXj.jpeg', 'خدماتنا تسقي لك عملك كالزرع  خدماتنا تسقي لك عملك كالزرع خدماتنا تسقي لك عملك كالزرع خدماتنا تسقي لك عملك كالزرع خدماتنا تسقي لك عملك كالزرع خدماتنا تسقي لك عملك كالزرع خدماتنا تسقي لك عملك كالزرع خدماتنا تسقي لك ع كالزرع', 1, '2017-04-10 12:31:52', '2017-04-10 12:31:52');

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`id`, `title`, `slug`, `content`, `is_active`, `created_at`, `updated_at`) VALUES
(5, 'روئيتنا', 'roeytna', '<h2>على المدى المتوسط</h2><p>يعتقد مات أن كل هذه التكنولوجيات المتنوعة والتي ما تزال في مراحلها الأولى ستتحول لاحقًا إلى شيء مألوف ولكن غريب.</p><p>إن كلًا من مايكروسوفت وفيسبوك وجوجل وماجيك ليب (المدعومة من جوجل) تعمل على ابتكار أجهزة الواقع المعزز الخاصة بها، حيث ستجلب تلك الأجهزة صورًا ثلاثية الأبعاد مبهرة مباشرة إليك. ويقال إن أبل تعتزم المنافسة في هذا المضمار أيضًا.</p><p>كان أليكس كبمان &ndash; المسؤول عن مشروع Hololens في شركة مايكروسوفت &ndash; قد أكد لموقع بيزنس إنسايدر أن تكنولوجيا الواقع المعزز قد تحل محل الهواتف الذكية والتلفزيونات وأي جهاز له شاشة. فلا فائدة لأجهزة منفصلة للرد على مكالماتك والدردشة ومشاهدة التلفاز وممارسة الألعاب إذا كان من الممكن أن تمارس كل ذلك عبر عينيك.</p><figure contenteditable="false"><img src="https://www.sasapost.com/wp-content/uploads/4334237247_08af133b4b_b-1.jpg" data-lazy-src="https://www.sasapost.com/wp-content/uploads/4334237247_08af133b4b_b-1.jpg" data-attachment="127256" class="fr-fic fr-dii"></figure><p>وفي نفس الوقت &ndash; يضيف مات &ndash; تزداد أهمية ملحقات مثل مساعد أمازون Amazon Echo أو سماعة أبل AirPods. وكلما ازدادت فعالية أنظمة الذكاء الاصطناعي مثل Siri الخاص بأبل، لن يزداد فقط الحديث إلى الكمبيوتر، وإنما سيكون هناك رد من جانب الأخيرة.</p>', 1, '2017-04-06 11:20:39', '2017-04-09 09:19:24'),
(6, 'مميزتنا', 'mmyztna', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</p><p>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</p>', 1, '2017-04-11 17:17:09', '2017-04-11 17:17:09'),
(7, 'من نحن', 'mn-nhn', '<p><span style="font-family: Arial, Helvetica, sans-serif;"><strong>الوطن، ملجأ القلب والروح، والملاذ الآمن الذي يضمّ أبناءه ويصون كرامتهم وعزتهم، ويمنعهم من ذل التشرّد والحاجة؛ فالوطن أكبر من مجرد المساكن والبيوت والشوارع، بل هو الأهل والجيران والخلان، وهو المكان الذي تسكن إليه النفس، وترتاح وتهدأ، وهو أولى الأماكن بالحب والحنين، فحب الوطن بالفطرة، وليس تصنّعاً ولا تمثيلاً. لأنّ الوطن أكبر من كل شيء، فحمايته واجبٌ على جميع أبنائه، فهو أغلى من الروح والدماء والأبناء، لأن الإنسان لا يستطيع العيش دون وطنٍ يحفظ له كرامته وهيبته، ويقيه من الهوان والضياع، حتى أن حب الوطن ممتدٌ من الإيمان بالله تعالى، وقد أوصى الله سبحانه وتعالى ورسوله بالذود عن الحمى، والوقوف في وجه كلّ من يحاول النيل منه، وتخريبه، وإيقاع الدمار فيه، وجعل جزاء من يموت في سبيل الوطن، جنةً عرضها السماوات والأرض.</strong></span></p>', 1, '2017-04-12 12:21:14', '2017-04-12 13:03:54'),
(8, 'مقاله', 'mkalh', '<div class="new-tab-image" id="indepth-intro" style=''font-size: 16px;vertical-align: baseline;background: transparent;color: rgb(119, 119, 119);font-family: Greta, "arabic typesetting", serif;text-align: start;''><br class="Apple-interchange-newline"><div class="detailed-knlg-page-add" style="font-size: 16px;vertical-align: baseline;background: transparent;width: 1281.55px;"><header class="ta-padin ta-all d2-d3 da-padfull" id="indepth-article-header" style="font-size: 16px;vertical-align: baseline;background: rgba(250, 250, 250, 0.701961);width: 622px;"><h1 class="heading-story"><span style=''font-size: 1.8rem;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Bold, "arabic typesetting", serif;color: rgb(139, 139, 139);''>مقالة في الحرية</span></h1></header></div></div><div class="fixed" style="font-size: 18px;vertical-align: baseline;background: transparent;width: 182.297px;"><div class="page-section share clearfix" style="font-size: 1.4rem;vertical-align: baseline;background: transparent;">شارك</div><ul class="page-anchors" style="font-size: 1.2rem;vertical-align: baseline;background: transparent;"><li style="font-size: 12px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;"><img src="http://www.aljazeera.net/views/shared/shared/images/media/up.png" alt="icon" style="font-size: 12px; background: transparent; width: 16px;" class="fr-fic fr-dii">عد إلى الأعلى</li><li style="font-size: 12px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;"><img src="http://www.aljazeera.net/views/shared/shared/images/media/zoom-in.png" alt="icon" style="font-size: 12px; background: transparent; width: 16px;" class="fr-fic fr-dii">تكبير الخط</li><li style="font-size: 12px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;"><img src="http://www.aljazeera.net/views/shared/shared/images/media/zoom-out.png" alt="icon" style="font-size: 12px; background: transparent; width: 16px;" class="fr-fic fr-dii">تصغير الخط</li><li style="font-size: 12px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;"><img src="http://www.aljazeera.net/views/shared/shared/images/media/send.png" alt="icon" style="font-size: 12px; background: transparent; width: 16px;" class="fr-fic fr-dii">إرسال إلى صديق</li><li style="font-size: 12px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;"><img src="http://www.aljazeera.net/views/shared/shared/images/media/print.png" alt="icon" style="font-size: 12px; background: transparent; width: 16px;" class="fr-fic fr-dii">طباعة</li></ul><div class="related_topics_areas" style="font-size: 18px;vertical-align: baseline;background: transparent;"><p class="meta"><span style=''font-size: 1.2rem;vertical-align: baseline;background: transparent;color: rgb(136, 136, 136);font-family: Al-Jazeera-Arabic-Regular, "arabic typesetting", serif;''>القسم:كتب</span></p><p class="meta">المناطق:<span class="Apple-converted-space" style=''font-size: 1.2rem;vertical-align: baseline;background: transparent;color: rgb(136, 136, 136);font-family: Al-Jazeera-Arabic-Regular, "arabic typesetting", serif;''>&nbsp;</span>العالم</p></div></div><div class="show_desktop related_items" style="font-size: 18px;vertical-align: baseline;background: transparent;width: 378.828px;"><h3><span style=''font-size: 16px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Bold, "arabic typesetting", serif;''>متعلقات</span></h3><div style="font-size: 18px;vertical-align: baseline;background: transparent;"><ul class="relatedItem list list-mod" style=''font-size: 1.4rem;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Bold, "arabic typesetting", serif;color: rgb(0, 69, 147);''><li style="font-size: 14px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;">سوريا.. درب الآلام نحو الحرية</li><li style="font-size: 14px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;">مانديلا .. أيقونة الصمود</li><li style="font-size: 14px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;">مفهوم الحريات</li><li style="font-size: 14px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;">أسئلة الحداثة في الفكر العربي</li><li style="font-size: 14px;vertical-align: baseline;background: transparent;font-family: Al-Jazeera-Arabic-Regular;">جرائم الصحافة</li></ul></div></div><div class="rs_skip rsbtn rs_preserve" id="readspeaker_button1" style="font-size: 18px;vertical-align: baseline;background: transparent;width: 841.844px;"><span class="rsbtn_left rsimg rspart" style=''font-size: 12px;vertical-align: baseline;background: url("img/rs_button.png") 0px 0px no-repeat scroll transparent;''><span class="rsbtn_text" style=''font-size: 12px;vertical-align: baseline;background: url("img/rs_button.png") 0px -641px no-repeat scroll transparent;font-family: "Lucida Grande", "Lucida Sans", Lucida, sans-serif;''><span style="font-size: 12px;vertical-align: baseline;background: transparent;">استمع</span></span></span></div><div id="DynamicContentContainer" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><p><span style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><strong style="font-size: 18px;vertical-align: baseline;background: transparent;"><br>عرض/المركز العربي للأبحاث ودراسة السياسات</strong></span><br>صدر حديثا عن المركز العربي للأبحاث ودراسة السياسات<span class="Apple-converted-space">&nbsp;</span><span class="cantweet" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>كتاب جديد للدكتور عزمي بشارة بعنوان &quot;مقالة في الحرية&quot;</span>، تناول فيه قضية الحرية باعتبارها مسألة متعلقة بالأخلاق، منطلقا من شرط المسؤولية الأخلاقية، ومعتبرا الحرية قيمة في حد ذاتها؛ حيث تقوم فرضية مقالته على أن الحرية كقيمة تعني الحرية بوصفها حريات، &quot;أما الحرية الأنطولوجية والحرية المطلقة والحرية المجردة فليست قيما&quot;، كما يقول.<br><br>هذا الكتاب هو كتاب نظري فلسفي بامتياز، ولكنه في مرحلة ما ينتقد المعالجات الفلسفية وينتقل إلى الفلسفة العملية. يصف بشارة الحرية بأنها موضوع خطر ومصيري للإنسان والمجتمع، &quot;لا يقتصر الخوض فيه على تحديد الحريات كقيمة ومعيار تقاس أخلاقية الفعل بموجبهما فحسب، بل يفترض أن يتجاوز ذلك ليتضمن وعي الإنسان بالخيارات في مرحلة معينة، وضمن أوضاع تاريخية معطاة، ومدى دفع أي خيار منها المجتمع تجاه تحقيق الحريات وضمانها، والمخاطر الكامنة في الخيارات التي لا تُحسب عواقبها على نحو صحيح، ومنها الإضرار بقضية الحرية ذاتها&quot;.<br><br>في الفصل الأول من الكتاب، وعنوانه دلالات اللفظ ليست غريبة عن العربية، يقتفي بشارة أثر لفظ الحرية ودلالاته بالعربية، ويرسم له مكامن تحد، أهمها القدرة على مغادرة النقاش الفلسفي عن الحرية، والانطلاق إلى مسألة الحريات وشروط تحقيقها وحدودها وعوائق تحققها في المجتمعات والدول العربية.<span class="Apple-converted-space">&nbsp;</span><br><br>وإذ ينتقل إلى تصنيف الحريات في العصر الراهن، يرى أن هذا التصنيف حاصل بين حريات شخصية ومدنية وسياسية، &quot;ويمكننا موضعة حرية الفكر والإبداع الفني والأدبي بين المدني والسياسي، حيث لا يزال النضال لتحقيقها مستمرا، أو بين الحريات الشخصية والمدنية، حيث ما عادت ممارستها قضية سياسية&quot;.</p><div style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><div style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><div style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><table style="width: 100%;"><tbody style="font-size: 18px;vertical-align: baseline;background: transparent;"><tr><td style="vertical-align: middle;text-align: right;"><span style=''font-size: 18px;vertical-align: middle;background: rgb(192, 192, 192);color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><span class="magnify_image" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><img src="http://www.aljazeera.net/file/getcustom/c92d8aee-8f29-40b9-9e30-d3d94a22da98/3f8d3525-d31a-4b4d-8b80-7def7a5301d4" border="0" style="font-size: 18px; background: transparent; width: auto;" class="fr-fic fr-dii"></span></span></td></tr><tr><td style="vertical-align: baseline;text-align: center;"><span dir="rtl" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><span style="font-size:13px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Arial, sans-serif;">-العنوان:<span class="Apple-converted-space">&nbsp;</span></span><span style="font-size:13px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Arial, sans-serif;">مقالة في الحرية<br>-المؤلف: الدكتور عزمي بشارة<br>-عدد الصفحات: 208 صفحة.<br>-الناشر:</span><span style="font-size:13px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Arial, sans-serif;"><span class="Apple-converted-space">&nbsp;</span></span><span style="font-size:13px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Arial, sans-serif;">المركز العربي للأبحاث ودراسة السياسات<br>-الطبعة: الأولى، 2016</span></span></td></tr></tbody></table></div><p><span style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>ويتابع لافتا إلى إمكان السعي للحريات وتحقيقها وصونها بوصفها &quot;حقوق المواطنة&quot; في إطار الدولة فحسب، &quot;أما الجماعة القائمة خارج الدولة فتحوي السلطة السياسية في باطنها هي ذاتها، ولا حقوق فيها للفرد بما هو فرد، بل استحقاقات وواجبات تقابلها امتيازات مترتبة على انتمائه إليها ومكانته فيها&quot;.<br><br>في الفصل الثاني، ليس الطير حرا ولا يولد الناس أحرارا،<span class="Apple-converted-space">&nbsp;</span><span class="cantweet" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>لا يتفق بشارة مع من يظن أن الإنسان يولد حرا، وأن الحرية واحدة في مخلوفات الله كلها</span>. يقول: &quot;<span class="cantweet" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>على الرغم من حلم الإنسان بحرية الطيور، الأخيرة ليست حرة، على خلاف البشر</span>. ليست حرة، لا في الرفرفة بأجنحتها، ولا في تغريدها. أما البشر فيبحثون في النهاية عن حريتهم في مواضعها الأثيرة؛ النفس الإنسانية والمجتمع البشري المنظم. الحرية معرفة بالعقل والإرادة، مشروطةٌ بهما؛ ومن نافل القول إنهما لا يتوافران في كائنات لا تملك عقلا وإرادة، فلا حرية في الطبيعة&quot;.<img class="BackToTopImage fr-fil fr-dii" src="http://www.aljazeera.net/App_Themes/SharedImages/top-page.gif" border="0" align="left" style="font-size: 18px; background: transparent; width: auto;"><br><br><span class="cantweet" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>ينظر المؤلف إلى الحرية، وينظّر لها، باعتبارها قدرة على الحسم بين خيارات</span>، وهذه قدرة ملازمة للإنسان يمتاز بها عن باقي الكائنات، &quot;ولا حرية للكائنات التي تقع دون الإنسان أو فوقه، الأولى بحكم طبيعتها، والثانية بحكم تعريفها. وميزت الديانات التوحيدية الإنسان من الملائكة بهذه القدرة على الاختيار بين الخير والشر، وتجعل الإنسان كائنا أخلاقيا يُتوقع منه التمييز بينهما واختيار أحدهما، مثلما يُتوقع منه بصفته كائنا عاقلا أن يميز بين الصواب والخطأ أيضا&quot;.<br><br>أما الولادة حرا، فيردها بشارة إلى مفهوم آخر: &quot;نقصد أنه لم يولد عبدا&quot;، وهذه مقولةٌ صحيحة -بحسبه- موجهةٌ ضد العبودية بنفي طبيعيتها، &quot;لكن حتى شروط الحرية ومكوناتها، أي الوعي والإرادة، لا تتوافر في المولود البشري عند ولادته مباشرة&quot;. وتبقى الحرية في مقالة بشارة &quot;مشروطة بالوعي والإرادة والقدرة على تحمل مسؤولية القرار المستقل في مجال ما، كي يكون للاختيار علاقة بهذا المجال؛ إذ لا معنى للحرية في مجال ما إذا لم يكن للخيارات علاقة به&quot;.<br><br>يرى المؤلف في الفصل الثالث، وعنوانه ملاحظات فلسفية لازمة، أن لا علاقة لتحرر الإرادة الإنسانية من قدرية الإرادة الإلهية بتحرير العلم قوانين المادة منها، فـ &quot;هذان أمران مختلفان. ولا يمكن التعامل معهما بوصفهما متصلا واحدا، وعلى الإحداثيات نفسها، إلا إذا اعتُبرت الإرادة الإنسانية جزيئا من جزيئات هذه الآلة المادية الكونية الخاضعة لقوانينها. لكنها في هذه الحالة تتحرر من القدر الإلهي لتصبح محكومة بالضرورات الطبيعية، فتنتفي عنها الحرية تماما&quot;.<img class="BackToTopImage fr-fil fr-dii" src="http://www.aljazeera.net/App_Themes/SharedImages/top-page.gif" border="0" align="left" style="font-size: 18px; background: transparent; width: auto;"><br><br><span class="cantweet" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>يعتبر بشارة الفوضى الاجتماعية &quot;أشنع أنواع الاستبداد&quot;، لأنها تمنع ممارسة الحرية بالضرورة</span>، ولأن الحرية غير ممكنة التحقيق إن سيطر التعسف والفوضى على حياة البشر، لكنها قابلة للتحقق في ظل القوانين، على الرغم من قدرة هذه القوانين على حجب الحريات.<br><br></span></p><div style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><blockquote class="quote-pull tb3-tb4 tb-padin tb-floatleft da3-da4 da-padin da-floatleft" style=''font-size: 2.2rem;vertical-align: baseline;background: transparent;color: rgb(153, 153, 153);font-family: Al-Jazeera-Arabic-Light, "arabic typesetting", serif;width: 402.141px;''><p><span style=''font-size: 18px;vertical-align: baseline;background: rgb(230, 231, 231);color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>&quot;يعتبر بشارة الفوضى الاجتماعية &quot;أشنع أنواع الاستبداد&quot;، لأنها تمنع ممارسة الحرية بالضرورة، ولأن الحرية غير ممكنة التحقيق إن سيطر التعسف والفوضى على حياة البشر، لكنها قابلة للتحقق في ظل القوانين، على الرغم من قدرة هذه القوانين على حجب الحريات&quot;</span></p></blockquote></div><p><span style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>وبحسب المؤلف، يجد العلماء صعوبة في التعامل مع فكرة الحرية علميا، و&quot;ربما وجد الفيزيائيون لها فسحة في فيزياء الكم وغيرها، وفي امتناع بعض المجريات الطبيعية عن التنظيم إلا من خلال علم الإحصاء، وعلى درجة معينة من الاحتمال فحسب&quot;، مؤكدا أن لا وجود للحرية من دون وعي وإرادة، وأن ليست الحرية هي المقابل للعلية أو الحتمية. وفي رأي بشارة، لا وجود لحرية كوزمولوجية، ولا العلم رديف الحرية فلسفيا، ولا يؤدي عبور حاجز الفكر الديني إلى المجال العقلاني الوضعي إلى الحرية بالضرورة. يقول: &quot;إذا كانت الحرية موضوع تنظيرات فلسفية ذات معنى، فلا بد من أن تكون، في رأينا، تنظيرات فلسفية في مجال الأخلاق، وتتمحور حول مقولتين: شرط الأخلاق حرية الإرادة، والحرية قيمة&quot;.<br><br>في الفصل الرابع، الحرية: أهي سالبة وموجبة؟ يشدد المؤلف على أن الحرية مسؤولية، وعلى أن الإنسان مسؤول ليس عما اختاره وقام به فحسب، بل عن نتائج هذا الخيار أيضا.<br><br>يرى المؤلف أن الحرية السالبة لا تظهر بذاتها في التاريخ، لا كحرية اختيار مجردة، ولا بوصفها كسرا لأي قيد على هذه الحرية. ولا تظهر الحرية الموجبة وحدها من دون صيرورة تحرر، ويقول إن هذه التصنيفات بين حرية سالبة وموجبة هي تصنيفات عقلية لعناصر غير منفصلة في الظواهر الاجتماعية، لافتا إلى أن الحرية السالبة تنفي القيود، &quot;إنها التحرر من إكراه الإنسان على اتباع نمط حياة مفروض، ورفض تقييد حرية الحركة وتكميم الأفواه وحظر التجمع والسجن العشوائي. وهي إذا لم تشمل حرية الإنسان بأن يقوم بأفعال معينة تتجلى في حريات موجبة وتوفير الشروط كي يمارس هذه الحريات، فإنها تبقى مجردة&quot;.<br><br><span class="cantweet" style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>يعتبر بشارة أن الفصل بين التحرر والحريات مصطنع</span>، فالتحرر عنده انطلاق يحمل استعدادا للتضحية وفرحَ الانعتاق وقيمه الفاتنة، وينتج أدبا وفنا تحرريا، &quot;لكنه إذا لم يؤسس للحريات والحقوق والمسؤولية عن ممارستها، ينقلب غالبا إلى ضده. وهذا في حد ذاته ينفي التمييز بين الحرية الموجبة والحرية السالبة على المستوى التاريخي، وإن صح التمييز لحظيا وصدقه الناس&quot;.<br><br>يرصد بشارة الفصل الخامس، وعنوانه عن الانشغال الفلسفي بالحرية، للنقاش الفلسفي المستمر في شأن العلاقة بين حتمية العلية وحرية الإرادة، ومن ثم الحق في محاسبة الإنسان، ولا يرى تناقضا بين العلية والحرية، إذ يلخص حرية الإنسان بأنه في فعله نفسه يضيف سلسلة علية جديدة ناجمة عن الإرادة والفعل.<br><br>ولا يرى بشارة الحرية قضية نظرية فلسفية في الفكر الليبرالي المعاصر، بل يعدها مسألة سياسة ومجتمع واقتصاد.<br><br>يطرح بشارة في الفصل السادس، وعنوانه بعض الأسئلة العملية الكبرى، تساؤلات عدة، يجعلها منصات نقاشية لمسألة الحرية والحريات. يسأل: ما نوع الوعي اللازم لممارسة الحريات السياسية؟ أتشترط ممارستها بها أم أن ممارسة الحرية هي شرط لتطور هذا الوعي؟<img class="BackToTopImage fr-fil fr-dii" src="http://www.aljazeera.net/App_Themes/SharedImages/top-page.gif" border="0" align="left" style="font-size: 18px; background: transparent; width: auto;"><br><br>يقول بشارة إن الوصول إلى درجات عليا من الوعي ليس ممكنا من دون ممارسة حرية الرأي والتعبير والاجتماع والاتحاد وحق الوصول إلى المعلومات وغيرها من الحريات المدنية. فتوافر الحريات الفردية شرط الديمقراطية الليبرالية، وهي معطى يمثل جزء من حقوق المواطنة الحديثة.</span></p><div style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''><blockquote class="quote-pull tb3-tb4 tb-padin tb-floatleft da3-da4 da-padin da-floatleft" style=''font-size: 2.2rem;vertical-align: baseline;background: transparent;color: rgb(153, 153, 153);font-family: Al-Jazeera-Arabic-Light, "arabic typesetting", serif;width: 402.141px;''><p><span style=''font-size: 18px;vertical-align: baseline;background: rgb(230, 231, 231);color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>&quot;يقول بشارة إن الوصول إلى درجات عليا من الوعي ليس ممكنا من دون ممارسة حرية الرأي والتعبير والاجتماع والاتحاد وحق الوصول إلى المعلومات وغيرها من الحريات المدنية. فتوافر الحريات الفردية شرط للديمقراطية الليبرالية، وهي معطى يمثل جزء من حقوق المواطنة الحديثة&quot;</span></p></blockquote></div><p><span style=''font-size: 18px;vertical-align: baseline;background: transparent;color: rgb(51, 51, 51);font-family: Greta, "arabic typesetting", serif !important;''>ويسأل بشارة: إذا وقعت المفاضلة بين الاستقرار وحفظ الحياة من جهة والحرية من جهة أخرى، فأيهما نختار؟ إنه السؤال الشائع اليوم في المجتمعات العربية، فالأنظمة الحاكمة تميل إلى تخيير المحكومين بين الاستبداد والفوضى، &quot;لكنه، مع ذلك، سؤالٌ وهمي. فمن يعارض منح الناس الحريات المدنية والسياسية لا يدعي عادة تأييد الظلم، بل يقابل الحرية بقيمة أخرى مثل الوطنية أو الاستقرار والحفاظ على الأمن. فهو يتهم مطالب الحريات المدنية والسياسية بأنها مؤامرة خارجية.<br><br>ويسأل: هل ثمة علاقة بين الحريات الشخصية والحريات المدنية والسياسية؟ وهل تصح التضحية بالحريات المدنية والسياسية لغرض الحفاظ على الحريات الشخصية؟ وهل تحرير المجتمع والفرد من الإملاءات الدينية يكفي كي يمارس الإنسان حريته؟<br><br>يستمر بشارة في طرح الأسئلة: لا شك في أن أغلبية الدول العربية لا تتيح حرية التعبير عن الرأي، ولا سيما الرأي السياسي. فهل تعززت هذه الحرية مع انتشار وسائل التواصل الاجتماعي؟ وما العلاقة بين حرية الفرد وحرية الجماعة التي ينتمي إليها؟ ولمن الأولوية، للمساواة أم للحريات؟ ويوضح أن إطلاق الحريات للجميع من دون توافر المساواة في شروط تحقيقها &quot;يفرغها من أي مضمون عملي، ويحولها حريات نظرية غير ممارسة وغير ممكنة التحقيق&quot;.<br><br>في الفصل السابع والأخير، يحاول بشارة الإجابة عن سؤال طرحه في الفصل السابق حول العلاقة بين الحرية والعدالة، وعنوانه مداخلة بشأن العدالة.. سؤال في السياق العربي المعاصر.<br><br>يرى المؤلف أن لفظ العدالة كمصطلح، قبل أن يتبلور مفهوما في نظريات، &quot;يقع في حقول دلالية متقاطعة مثل التساوي والمعاملة بالمثل والملاءمة والاستقامة وغيرها من الدلالات التي يتضمنها اللفظ العربي؛ فلفظ &quot;عادَلَ&quot; بالعربية معناه ناظَرَ أو شابَهَ، ويعني أيضا وازَن، و&quot;عدّل&quot; الشيء أي صححه وجعله مستقيما، ومن هنا ارتباط مقولة معروفة تُنسب إلى عمر بن الخطاب {رضي الله عنه} عن تقويم الاعوجاج بالعدالة: &quot;من رأى منكم في اعوجاجا فليقومه&quot;. الاستقامة والعدالة في مقابل الظلم والاعوجاج. وكذلك نجد الزيغ والميلان والهوى، في مقابل العدل. فالعادل هو أيضا من لا يميل به الهوى حين يحكم بين خصمين، أو حين يطبق القواعد الاجتماعية المانعة الشر&quot;.<br><br>ثبت للمؤلف، من خلال نضال الشعوب ضد الظلم في التاريخ، أن الحرية شرط من شروط العدالة التي يتغير مفهوم الناس لها بالمعرفة والممارسة؛ &quot;ونظريا، أصبحت الحرية مركبا لا يمكن تجاهله عند الحديث عن العدالة لأسباب ثلاثة: أولا، ربما يكون تقييد الحرية أحد أهم التعبيرات عن اللامساواة في الحقوق؛ ثانيا، ربما ينتج من تقييد الحرية بحد ذاته عدم مساواة في الحقوق، ومن ثم يؤدي إلى انعدام العدالة، ولا سيما تلك العدالة التي تحاول فرض تصور عن السعادة قائم لدى من يفرضها؛ ثالثا، لأن الحرية أصبحت من الناحية الفكرية، وفي حاجات الناس وتوقعاتهم وتطلعاتهم، أحد الخيرات أو إحدى المنافع الاجتماعية والوطنية، فأصبح لزاما أيضا توزيعها توزيعا عادلا، حيث أصبحت الحرية السياسية حاجة إنسانية&quot;.</span></p></div></div></div>', 1, '2017-04-12 12:22:48', '2017-04-12 12:22:48');

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `ip`, `created_at`, `updated_at`) VALUES
(1, 'aliturki@gmail.com', NULL, '2017-04-03 22:00:00', '2017-04-03 22:00:00'),
(2, 'aliturki2020@gmail.com', NULL, '2017-04-06 07:00:28', '2017-04-06 07:00:28'),
(3, 'ali@ali.com', NULL, '2017-04-06 07:11:23', '2017-04-06 07:11:23'),
(4, 'barakata@name.com', NULL, '2017-04-06 07:12:16', '2017-04-06 07:12:16'),
(5, 'ali@ali.net', '127.0.0.1', '2017-04-06 07:13:40', '2017-04-06 07:13:40');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `speaker` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `speaker`, `content`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'محمد أحمد', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 10:54:19', '2017-04-04 10:54:19'),
(2, 'على تركي', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 10:54:59', '2017-04-04 10:54:59'),
(3, 'محمود', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 10:57:49', '2017-04-04 10:57:49'),
(4, 'محمد', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 10:58:41', '2017-04-04 10:58:41'),
(5, 'بودس', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 10:59:47', '2017-04-04 10:59:47'),
(6, 'على', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 0, '2017-04-04 11:04:16', '2017-04-04 11:04:16'),
(7, 'اسمك', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 11:05:44', '2017-04-04 11:05:44'),
(8, 'مدحت', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 11:07:07', '2017-04-04 11:07:07'),
(9, 'القائل', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.', 1, '2017-04-04 11:08:04', '2017-04-04 11:18:57');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `isAdmin`, `email`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'محمود برعي', 1, 'info@service.com', '$2y$10$3aXtGSBYeYlTukU4xmdMPuZQ4WsO7mEVYs9.zCRf89MoZRAcxoDLi', '1492176212-GIFskE.png', 'NC2uzsRi2mWW0gvooWOXjeavPc6DmRlW24duUlkruicA9vXsG6PwKZznn5qA', '2017-03-16 21:31:32', '2017-04-14 11:23:32'),
(2, 'Ali', 1, 'aliturki2020@gmail.com', '$2y$10$Vv8KhrLzofVdoGD/G29zv.M8ExpVkNWRqQyMMVHvj1lCikLj.ig02', '1492100659-VY2Y6i.png', NULL, '2017-04-13 14:19:03', '2017-04-13 14:24:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactuses`
--
ALTER TABLE `contactuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `static_pages_slug_unique` (`slug`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribes_email_unique` (`email`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contactuses`
--
ALTER TABLE `contactuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
