const { mix } = require('laravel-mix');


mix.js('resources/assets/backend/js/app.js', 'public/backend/js')
   .sass('resources/assets/backend/sass/app.scss', 'public/backend/css');

mix.sass('resources/assets/sass/app.scss', 'public/css');
mix.js('resources/assets/js/app.js', 'public/js');